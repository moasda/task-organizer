# Personal Kanban Task Organizer

## Description

*Personal Kanban Task Organizer* is a privacy-friendly and interactive Kanban
board to organize personal tasks. It provides an overview over all tasks,
allows to easily prioritize tasks, and change their status.

The application is optimized for user data privacy. It stores the task data in
a local JSON file and never establishes a cloud connection. Optionally, the
today's tasks can be visualized in any local calendar tool with iCalendar
support, e.g. Evolution, MS Outlook.

## Key features

- Task prioritization using time frames
  - Ideas (unscheduled)
  - This year
  - This quarter
  - This month
  - This week
  - Today
  - Blocked
  - Done
- Task status
  - Needs action
  - In process
  - Completed
  - Cancelled
- Due Date visualization: "⏱-3" means "three days before due date"
- Optimized on personal usage for a single user (e.g. a manager or a private
  project), it's not a collaboration tool for multi-user access
- Data privacy by design: Stores task data only on the local computer, never
  establishes a cloud connection
- UI languages: English, German, Dutch
- Platform: Linux and Windows operating systems (64 bit)
- Free/Libre and Open Source Software (FLOSS)

## Screenshot

<img src="images/screenshot.png" width="300" height="128" />

## Download & Installation

1. Download the DEB package or EXE file from the [releases page][releases].
   Currently, Debian based Linux (incl. Ubuntu) and Windows operating systems
   (64 bit) are supported.
2. Install the DEB package or run the EXE installer.
3. Select "Personal Kanban" in the start menu.

The application is self-contained, no external packages or libraries have to be
installed (the required Debian packages are usually already installed).

[releases]: https://gitlab.com/moasda/task-organizer/-/releases

## Build

Steps to compile the sources:

1. Install the [Lazarus IDE][lazarus].
2. Install the Lazarus package UniqueInstance.
   (Menu Package &rarr; Online Package Manager &rarr; UniqueInstance &rarr;
   Install &rarr; Rebuild Lazarus)
3. Check out the project from GitLab
4. Build and run the project

## Location of task file

The task data is stored in a local JSON file `tasks.json`, located in the
user's home folder.
This folder can be changed in the settings dialog.
For backups just make a copy of that JSON file.

## Licenses

*Personal Kanban Task Organizer* is licensed under GPL-3.0-only &ndash; see the
[LICENSE](LICENSE) file for details.

The library [*JsonTools*][JsonTools] is licensed under GPLv3 and LGPLv3
([source code][JsonToolsSource]).

The library [*Synapse*][synapse] is licensed under a BDS style license
([source code][BDS-style]).

The required development and build tools [Lazarus IDE][lazarus] and [Free
Pascal Compiler][free-pascal] are free and open-source software (FOSS) as well.

The [*Tango Icon Library*][tango-icon-library] is released as Public Domain.

[BDS-style]: https://sourceforge.net/p/synalist/code/HEAD/tree/trunk/
[free-pascal]: https://freepascal.org/
[JsonTools]: https://www.getlazarus.org/json/
[JsonToolsSource]: https://github.com/sysrpl/JsonTools/
[lazarus]: https://www.lazarus-ide.org/
[synapse]: https://www.ararat.cz/synapse/
[tango-icon-library]: http://tango.freedesktop.org/Tango_Icon_Library
