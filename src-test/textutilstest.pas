{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit TextUtilsTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type
  TTextUtilsTest = class(TTestCase)
    procedure TestIsCodepointInRange;
    procedure TestWordPosBeforeCursor;
    procedure TestDateTime2String;
    procedure TestString2DateTime;
    procedure TestEncodeLineBreaks;
    procedure TestDecodeLineBreaks;
  public
    class procedure AssertEqualsDateTime(Date: TDateTime; Year, Month,
      Day, Hour, Minute, Second, MilliSecond: Word);
  end;

implementation

uses TextUtils;

procedure TTextUtilsTest.TestIsCodepointInRange;
begin
  // basic tests
  AssertTrue(IsCodepointInRange('abc', 0, ['a']));
  AssertTrue(IsCodepointInRange('abc', 0, ['a'..'c']));
  AssertTrue(IsCodepointInRange('abc', 2, ['a'..'c']));

  // char not in set
  AssertFalse(IsCodepointInRange('abc', 0, []));
  AssertFalse(IsCodepointInRange('abc', 0, ['b'..'c']));

  // codepoint not in string
  AssertFalse(IsCodepointInRange('', 0, []));
  AssertFalse(IsCodepointInRange('', 5, []));
  AssertFalse(IsCodepointInRange('abc', 3, ['a'..'c']));
end;

procedure TTextUtilsTest.TestWordPosBeforeCursor;
begin
  // basic tests
  AssertEquals(0, WordPosBeforeCursor('', 0));
  AssertEquals(0, WordPosBeforeCursor('abc', 0));
  AssertEquals(0, WordPosBeforeCursor('abc', 2));
  AssertEquals(0, WordPosBeforeCursor('abc', 3));
  AssertEquals(0, WordPosBeforeCursor('abc def', 3));

  // in second word
  AssertEquals(4, WordPosBeforeCursor('abc def', 5));
  AssertEquals(4, WordPosBeforeCursor('abc def', 6));
  AssertEquals(4, WordPosBeforeCursor('abc def', 7));
  AssertEquals(4, WordPosBeforeCursor('abc def ghi', 7));
  AssertEquals(5, WordPosBeforeCursor('abc  def  ghi', 8));

  // more spaces
  AssertEquals(5, WordPosBeforeCursor('abc  def', 6));
  AssertEquals(9, WordPosBeforeCursor('abc  def ghi', 11));
  AssertEquals(9, WordPosBeforeCursor('abc  def ghi', 12));

  // space before cursor -> jump to beginning of left word
  AssertEquals(0, WordPosBeforeCursor('abc def', 4));
  AssertEquals(5, WordPosBeforeCursor('abc  def  ghi', 10));
  AssertEquals(5, WordPosBeforeCursor('abc  def  ghi', 9));

  // unicode characters
  AssertEquals(4, WordPosBeforeCursor('abc 漢語', 5));
  AssertEquals(4, WordPosBeforeCursor('abc 漢語', 6));

  // line breaks
  AssertEquals(3, WordPosBeforeCursor('abc'#13'def', 4));
  AssertEquals(4, WordPosBeforeCursor('abc'#13'def', 5));
  AssertEquals(4, WordPosBeforeCursor('abc'#13'  def', 6));
  AssertEquals(4, WordPosBeforeCursor('abc '#13'def', 5));
  AssertEquals(4, WordPosBeforeCursor('abc'#13#13'def', 5));
  AssertEquals(5, WordPosBeforeCursor('abc'#13#13'def', 6));
  AssertEquals(3, WordPosBeforeCursor('abc'#13#10'def', 5));
  AssertEquals(3, WordPosBeforeCursor('abc'#10#13'def', 5));
  AssertEquals(5, WordPosBeforeCursor('abc'#13#10#13#10'def', 7));
end;

procedure TTextUtilsTest.TestDateTime2String;
var Date: TDateTime;
begin
  Date := EncodeDate(2021, 01, 30) + EncodeTime(01, 02, 03, 04);
  AssertEquals('2021-01-30T01:02:03Z', DateTime2String(Date));

  // without time
  Date := EncodeDate(2021, 01, 30);
  AssertEquals('2021-01-30T00:00:00Z', DateTime2String(Date));
end;

procedure TTextUtilsTest.TestString2DateTime;
var Date: TDateTime;
begin
  Date := String2DateTime('2021-01-30T01:02:03Z');
  AssertEqualsDateTime(Date, 2021, 01, 30, 01, 02, 03, 00);
end;

procedure TTextUtilsTest.TestEncodeLineBreaks;
begin
  AssertEquals('', EncodeLineBreaks(''));
  AssertEquals('abc', EncodeLineBreaks('abc'));
  AssertEquals('abc\ndef\nghi', EncodeLineBreaks('abc'#10'def'#10'ghi'));
  AssertEquals('abc\\ndef', EncodeLineBreaks('abc\ndef'));
  AssertEquals('ab\\zc\ndef\ng\\nhi', EncodeLineBreaks('ab\zc'#10'def'#10'g\nhi'));
end;

procedure TTextUtilsTest.TestDecodeLineBreaks;
begin
  AssertEquals('', DecodeLineBreaks(''));
  AssertEquals('abc', DecodeLineBreaks('abc'));
  AssertEquals('abc'#10'def'#10'ghi', DecodeLineBreaks('abc\ndef\nghi'));
  AssertEquals('abc\ndef', DecodeLineBreaks('abc\\ndef'));
  AssertEquals('ab\zc'#10'def'#10'g\nhi', DecodeLineBreaks('ab\\zc\ndef\ng\\nhi'));
  AssertEquals('abc\', DecodeLineBreaks('abc\')); // robustness
end;

class procedure TTextUtilsTest.AssertEqualsDateTime(Date: TDateTime;
  Year, Month, Day, Hour, Minute, Second, MilliSecond: Word);
var YearDecode, MonthDecode, DayDecode,
    HourDecode, MinuteDecode, SecondDecode, MilliSecondDecode: Word;
begin
  DecodeDate(Date, YearDecode, MonthDecode, DayDecode);
  DecodeTime(Date, HourDecode, MinuteDecode, SecondDecode, MilliSecondDecode);
  AssertEquals('decode year', Year, YearDecode);
  AssertEquals('decode month', Month, MonthDecode);
  AssertEquals('decode day', Day, DayDecode);
  AssertEquals('decode hour', Hour, HourDecode);
  AssertEquals('decode minute', Minute, MinuteDecode);
  AssertEquals('decode second', Second, SecondDecode);
  AssertEquals('decode millisecond', MilliSecond, MilliSecondDecode);
end;

initialization
  RegisterTest(TTextUtilsTest);
end.

