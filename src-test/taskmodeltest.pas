{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit TaskModelTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, TaskModel;

type

  TTaskModelTest = class(TTestCase)
    procedure TestContainsText;
    procedure TestCompareTitle;
  end;

  TTaskTest = class(TTestCase)
    procedure TestUpdateTask;
    class procedure AssertEqualsTaskStatus(Expected, Actual: TTaskStatus);
  end;

  TTaskManagerTest = class(TTestCase)
    procedure TestAddStandardKanbanGroups;
    procedure TestAddTask;
    procedure TestDeleteTask;
    procedure TestGetTask;
    procedure TestGetTasks;
    procedure TestGetICalendarTasks;
    procedure TestReadFromFile10;
    procedure TestReadFromFile20;
    procedure TestReadFromFile21;
    procedure TestWriteToFile;
  end;

implementation

uses TypInfo, TextUtilsTest;

procedure TTaskModelTest.TestContainsText;
var Task: TTask;
begin
  Task := TTask.Create;
  AssertFalse(ContainsText('abc', Task));

  Task.Title := 'myabcdef';
  AssertTrue(ContainsText('abc', Task));

  Task.Title := '';
  Task.Details := 'myabcdef';
  AssertTrue(ContainsText('abc', Task));

  Task.Free;
end;

procedure TTaskModelTest.TestCompareTitle;
var Task1, Task2: TTask;
begin
  Task1 := TTask.Create;
  Task2 := TTask.Create;
  AssertEquals(0, CompareTitle(Task1, Task2));

  Task1.Title := 'a';
  Task2.Title := 'b';
  AssertEquals(-1, CompareTitle(Task1, Task2));

  Task1.Title := 'b';
  Task2.Title := 'a';
  AssertEquals(1, CompareTitle(Task1, Task2));

  Task1.Free;
  Task2.Free;
end;

procedure TTaskTest.TestUpdateTask;
var Task1, Task2: TTask;
begin
  Task1 := TTask.Create;
  Task1.Id := 1;
  Task1.CreatedDate := 0;

  Task2 := TTask.Create;
  Task2.Id := 2;
  Task2.Title := 'Title 2';
  Task2.Details := 'Details 2';
  Task2.CreatedDate := 101;
  Task2.LastModifiedDate := 102;
  Task2.StartDate := 103;
  Task2.DueDate := 104;
  Task2.CompletedDate := 105;
  Task2.Status := Completed;
  Task2.KanbanGroupIndex := 7;

  Task1.UpdateTask(Task2);
  AssertEquals(1, Task1.Id); // field not changed
  AssertEquals('Title 2', Task1.Title);
  AssertEquals('Details 2', Task1.Details);
  AssertEquals(0, Task1.CreatedDate); // field not changed
  AssertEquals(102, Task1.LastModifiedDate);
  AssertEquals(103, Task1.StartDate);
  AssertEquals(104, Task1.DueDate);
  AssertEquals(105, Task1.CompletedDate);
  AssertEqualsTaskStatus(Completed, Task1.Status);
  AssertEquals(7, Task1.KanbanGroupIndex);

  Task1.Free;
  Task2.Free;
end;

class procedure TTaskTest.AssertEqualsTaskStatus(Expected, Actual: TTaskStatus);
var Msg: String;
begin
  Msg := 'Expected: ' + GetEnumName(TypeInfo(TTaskStatus), Ord(Expected))
      + ', but actual: ' + GetEnumName(TypeInfo(TTaskStatus), Ord(Actual));
  AssertTrue(Msg, Expected = Actual);
end;

procedure TTaskManagerTest.TestAddStandardKanbanGroups;
var TaskManager: TTaskManager;
    I: Integer;
    KanbanGroup: TKanbanGroup;
    NumCloseTasks, NumICalendarExport: Integer;
begin
  TaskManager := TTaskManager.Create;

  AssertEquals(8, TaskManager.GetKanbanGroups.Count);
  NumCloseTasks := 0;
  NumICalendarExport := 0;
  for I := 0 to TaskManager.GetKanbanGroups.Count - 1 do
  begin
    KanbanGroup := TaskManager.GetKanbanGroups[I];
    AssertEquals(I, KanbanGroup.Index);
    if KanbanGroup.CloseTasks then
    begin
      NumCloseTasks := NumCloseTasks + 1;
    end;
    if KanbanGroup.ICalendarExport then
    begin
      NumICalendarExport := NumICalendarExport + 1;
    end;
  end;
  AssertTrue(NumCloseTasks >= 1);
  AssertTrue(NumICalendarExport >= 1);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestAddTask;
var TaskManager: TTaskManager;
    Task: TTask;
    TaskId: Integer;
begin
  TaskManager := TTaskManager.Create;

  Task := TTask.Create;
  AssertEquals(0, Task.Id); // before AddTask
  TaskId := TaskManager.AddTask(Task);
  AssertTrue(TaskId <> 0); // after AddTask
  AssertEquals(TaskId, Task.Id);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestDeleteTask;
var TaskManager: TTaskManager;
    Task: TTask;
    TaskId: Integer;
begin
  TaskManager := TTaskManager.Create;

  Task := TTask.Create;
  TaskId := TaskManager.AddTask(Task);
  AssertNotNull(TaskManager.GetTask(TaskId)); // before DeleteTask
  TaskManager.DeleteTask(TaskId);
  AssertNull(TaskManager.GetTask(TaskId)); // after DeleteTask

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestGetTask;
var TaskManager: TTaskManager;
    Task: TTask;
    TaskId: Integer;
begin
  TaskManager := TTaskManager.Create;
  AssertNull(TaskManager.GetTask(1));

  Task := TTask.Create;
  Task.Title := 'My Task';
  TaskId := TaskManager.AddTask(Task);
  AssertEquals('My Task', TaskManager.GetTask(TaskId).Title);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestGetTasks;
var TaskManager: TTaskManager;
    Task: TTask;
begin
  TaskManager := TTaskManager.Create;
  AssertEquals(0, TaskManager.GetTasks.Count);

  Task := TTask.Create;
  TaskManager.AddTask(Task);
  AssertEquals(1, TaskManager.GetTasks.Count);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestGetICalendarTasks;
var TaskManager: TTaskManager;
    Task: TTask;
begin
  TaskManager := TTaskManager.Create;
  AssertEquals(0, TaskManager.GetICalendarTasks.Count);

  TaskManager.GetKanbanGroups[0].ICalendarExport := True;
  Task := TTask.Create;
  Task.KanbanGroupIndex := 0;
  TaskManager.AddTask(Task);

  TaskManager.GetKanbanGroups[1].ICalendarExport := True;
  Task := TTask.Create;
  Task.KanbanGroupIndex := 1;
  TaskManager.AddTask(Task);

  TaskManager.GetKanbanGroups[2].ICalendarExport := False;
  Task := TTask.Create;
  Task.KanbanGroupIndex := 2;
  TaskManager.AddTask(Task);

  AssertEquals(2, TaskManager.GetICalendarTasks.Count);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestReadFromFile10;
var TaskManager: TTaskManager;
    Task: TTask;
begin
  TaskManager := TTaskManager.Create;
  AssertEquals(0, TaskManager.GetTasks.Count);

  TaskManager.ReadFromFile('../../src-test/tasks10.json');
  AssertEquals(2, TaskManager.GetTasks.Count);

  Task := TaskManager.GetTasks[0];
  AssertEquals('Title 1', Task.Title);
  AssertEquals('Details 1', Task.Details);
  AssertEquals(0, Task.KanbanGroupIndex);

  Task := TaskManager.GetTasks[1];
  AssertEquals('Title 2', Task.Title);
  AssertEquals(' Details 2'#10'with line breaks'#10'€§$".'#10, Task.Details);
  AssertEquals(1, Task.KanbanGroupIndex);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestReadFromFile20;
var TaskManager: TTaskManager;
    Task: TTask;
begin
  TaskManager := TTaskManager.Create;
  AssertEquals(0, TaskManager.GetTasks.Count);

  TaskManager.ReadFromFile('../../src-test/tasks20.json');
  AssertEquals(9, TaskManager.GetTasks.Count);

  Task := TaskManager.GetTask(1);
  AssertEquals('Title 1', Task.Title);
  AssertEquals('Details 1', Task.Details);
  TTaskTest.AssertEqualsTaskStatus(NeedsAction, Task.Status);
  AssertEquals(0, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(2);
  AssertEquals('Title 2', Task.Title);
  AssertEquals(' Details 2'#10'with line breaks'#10'€§$".'#10, Task.Details);
  AssertEquals(1, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(3);
  AssertEquals('Title 3 testäöüßéè,.-', Task.Title);
  AssertEquals(2, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(4);
  AssertEquals(3, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(5);
  AssertEquals(4, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(6);
  AssertEquals(5, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(7);
  TTaskTest.AssertEqualsTaskStatus(InProcess, Task.Status);
  AssertEquals(6, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(8);
  TTextUtilsTest.AssertEqualsDateTime(Task.CreatedDate,      2021, 01, 01, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.LastModifiedDate, 2021, 01, 02, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.StartDate,        2021, 01, 03, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.DueDate,          2021, 01, 04, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.CompletedDate,    2021, 01, 05, 01, 02, 03, 00);
  TTaskTest.AssertEqualsTaskStatus(Completed, Task.Status);
  AssertEquals(7, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(9);
  TTaskTest.AssertEqualsTaskStatus(Cancelled, Task.Status);
  AssertEquals(7, Task.KanbanGroupIndex);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestReadFromFile21;
var TaskManager: TTaskManager;
    KanbanGroup: TKanbanGroup;
    Task: TTask;
begin
  TaskManager := TTaskManager.Create;
  AssertEquals(0, TaskManager.GetTasks.Count);

  TaskManager.ReadFromFile('../../src-test/tasks21.json');
  AssertEquals(3, TaskManager.GetKanbanGroups.Count);
  AssertEquals(3, TaskManager.GetTasks.Count);

  KanbanGroup := TaskManager.GetKanbanGroups[0];
  AssertEquals(0, KanbanGroup.Index);
  AssertEquals('Kanban Group 0', KanbanGroup.Title);
  AssertFalse(KanbanGroup.CloseTasks);
  AssertFalse(KanbanGroup.ICalendarExport);

  KanbanGroup := TaskManager.GetKanbanGroups[1];
  AssertEquals(1, KanbanGroup.Index);
  AssertEquals('Kanban Group 1', KanbanGroup.Title);
  AssertFalse(KanbanGroup.CloseTasks);
  AssertTrue(KanbanGroup.ICalendarExport);

  KanbanGroup := TaskManager.GetKanbanGroups[2];
  AssertEquals(2, KanbanGroup.Index);
  AssertEquals('Kanban Group 2', KanbanGroup.Title);
  AssertTrue(KanbanGroup.CloseTasks);
  AssertFalse(KanbanGroup.ICalendarExport);

  Task := TaskManager.GetTask(1);
  AssertEquals('Title 1', Task.Title);
  AssertEquals('Details 1', Task.Details);
  TTaskTest.AssertEqualsTaskStatus(NeedsAction, Task.Status);
  AssertEquals(0, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(2);
  AssertEquals('Title 2', Task.Title);
  AssertEquals(' Details 2'#10'with line breaks'#10'€§$".'#10, Task.Details);
  TTaskTest.AssertEqualsTaskStatus(InProcess, Task.Status);
  AssertEquals(1, Task.KanbanGroupIndex);

  Task := TaskManager.GetTask(3);
  AssertEquals('Title 3 testäöüßéè,.-', Task.Title);
  TTextUtilsTest.AssertEqualsDateTime(Task.CreatedDate,      2021, 01, 01, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.LastModifiedDate, 2021, 01, 02, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.StartDate,        2021, 01, 03, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.DueDate,          2021, 01, 04, 01, 02, 03, 00);
  TTextUtilsTest.AssertEqualsDateTime(Task.CompletedDate,    2021, 01, 05, 01, 02, 03, 00);
  TTaskTest.AssertEqualsTaskStatus(Completed, Task.Status);
  AssertEquals(2, Task.KanbanGroupIndex);

  TaskManager.Free;
end;

procedure TTaskManagerTest.TestWriteToFile;
var TaskManager: TTaskManager;
    Task: TTask;
    StringList: TStringList;
    FileContent: String;
begin
  TaskManager := TTaskManager.Create;

  Task := TTask.Create;
  Task.Title := 'Task Title';
  Task.Details := 'Task Details';
  Task.Status := TTaskStatus.InProcess;
  Task.KanbanGroupIndex := 6;
  TaskManager.AddTask(Task);
  TaskManager.WriteToFile('TestWriteToFile.json');

  StringList := TStringList.Create;
  StringList.LoadFromFile('TestWriteToFile.json');
  FileContent := StringList.Text;
  AssertTrue(FileContent.Contains('Task Title'));
  AssertTrue(FileContent.Contains('Task Details'));

  TaskManager.Free;
end;

initialization
  RegisterTest(TTaskModelTest);
  RegisterTest(TTaskTest);
  RegisterTest(TTaskManagerTest);
end.

