# Changelog

## 2.6.1 (2024-01-01)

- Bugfix: Show column titles in task edit dialog
- Update copyright year
- Upgrade build script to debian:bookworm-slim

## 2.6.0 (2023-04-29)

- Change option title to show old tasks
- Update translation
- Upgrade build script to Lazarus 2.2.6

## 2.5.0 (2023-02-26)

- Add work folder selection in settings dialog
- Drop command line support for work folder
- Store settings in official folder ("~/.config/Personal Kanban Task Organizer/" or
  "C:\Users\user\AppData\Local\Personal Kanban Task Organizer\")
- Enable DPI scaling per monitor
- Upgrade build script to Inno Setup 6.2.2

## 2.4.0 (2022-12-03)

- Add context menu item for columns to show/hide old tasks
- Hide old tasks by LastModifiedDate instead of CompletedDate
- Bugfix: Update column titles after language change (issue #6)
- Upgrade build script to Lazarus 2.2.4

## 2.3.0 (2022-06-02)

- Add Dutch language (contributed by @brenthuisman in issue #6)
- Upgrade build script to Lazarus 2.2.2 + FreePascal 3.2.2 + Inno Setup 6.2.1

## 2.2.0 (2021-12-26)

- Add task template for new tasks (as suggested by @whew in issue #2)

## 2.1.0 (2021-12-25)

- Hiding old tasks can be turned off
- Small improvements

## 2.0.3 (2021-07-31)

- Add icons to menu items

## 2.0.2 (2021-07-30)

- Add edit dialog for Kanban Group settings
- Small refactoring

## 2.0.1 (2021-07-29)

- Add Kanban Group export for iCalendar to settings dialog

## 2.0.0 (2021-07-26)

- Make Kanban columns configurable

## 1.3.25 (2021-06-29)

- Bugfix: Hide old tasks in done column
- Bugfix: Show correct completed date in task edit dialog

## 1.3.24 (2021-06-12)

- Extend logging
- Update license hint in some files

## 1.3.23 (2021-05-29)

- Highlight new added task
- Bugfix: Show new task in correct column if the kanban group is modified

## 1.3.22 (2021-05-26)

- Settings dialog: Ok button enabled even if font is not configured

## 1.3.21 (2021-05-22)

- Clear search text by Esc key even if cursor is not in search bar

## 1.3.20 (2021-05-12)

- Make GPL-3.0-only licensing more clear

## 1.3.19 (2021-05-10)

- Upgrade to Lazarus 2.0.12

## 1.3.18 (2021-05-05)

- Don't show due symbol for done tasks

## 1.3.17 (2021-05-04)

- Show remaining days number in bigger letters and highlight background
- Keep task selection on the board after task edit

## 1.3.16 (2021-05-03)

- Show symbol or number of remaining days for tasks with due date
- Optimize Linux start menu entry

## 1.3.15 (2021-05-02)

- Reimplement task board to fix ordering issues

## 1.3.14 (2021-04-26)

- Store tasks file and settings file by default in user's home folder
- This folder can be overridden by a command line parameter
- Store settings in file .personal-kanban (Linux) or personal-kanban.ini (Windows)

## 1.3.13 (2021-04-24)

- Create Windows installer

## 1.3.12 (2021-04-24)

- Create Debian package
- Use personal-kanban.ini instead of kanban.ini for settings

## 1.3.11 (2021-04-07)

- Fix empty due date picker in task edit dialog
- Small improvements

## 1.3.10 (2021-04-02)

- Allow to remove date values
- Task ID can be selected and copied
- Add iCalendar export hint
- Update translations

## 1.3.9 (2021-03-24)

- Don't mark done tasks yellow

## 1.3.8 (2021-03-16)

- Add transparency configuration to settings dialog
- Fix main window transparency on Linux

## 1.3.7 (2021-03-13)

- Add license note to source files

## 1.3.6 (2021-03-03)

- Highlight task with thick border while being edited
- Show server URL in monospace font
- Small layout improvements

## 1.3.5 (2021-02-27)

- Mark tasks yellow after due date
- Mark tasks red during editing

## 1.3.4 (2021-02-20)

- Restore minimized window when user tries to start a second instance

## 1.3.3 (2021-02-09)

- Move cursor to end of description text in task edit dialog.

## 1.3.2 (2021-01-30)

- Ensure that the task edit dialog is fully visible on the screen.
- Bugfix: After modifying a task the search filter was not applied.

## 1.3.1 (2021-01-22)

- Add URL copy button in settings dialog
- Highlight selected task
- Allow only one instance at the same time
- Change Ctrl+Backspace handling in input fields
- Small UI improvements

## 1.3.0 (2021-01-17)

- New logo
- UI colors improved
- Hide completed tasks older than 7 days (can be configured)
- Add more task attributes for better iCalendar integration
- Bugfix: After drag&drop of tasks the order was wrong sometimes

## 1.2.2 (2020-12-20)

- iCalendar: Change times to start dynamically with current hour
- Remove start time configuration from settings dialog
- Upgrade build script to Lazarus 2.0.10 + FreePascal 3.2.0
- Small fixes

## 1.2.1 (2020-12-13)

- Improve layout in main window
- Fix Ctrl+Backspace unicode handling
- iCalendar: Change times from local to UTC
- iCalendar: Change line delimiter to CRLF according to specification
- Small fixes

## 1.2.0 (2020-12-08)

- Add support for iCalendar synchronization
- Fix memory leaks

## 1.1.1 (2020-11-15)

- Create ZIP file with executable and language files

## 1.1.0 (2020-09-12)

- Use GitLab CI (`.gitlab-ci.yml`) to automatically build the executable file
- Add files README.md, LICENSE, and CHANGELOG.md

## 1.0.14 (2020-06-11)

- Initial release
