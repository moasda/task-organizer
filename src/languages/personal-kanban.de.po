msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"X-Generator: Poedit 2.4.2\n"

#: mainwindow.saddtaskbuttonhint
#, object-pascal-format
msgid "Add task to %s"
msgstr "Aufgabe hinzufügen zu %s"

#: mainwindow.sdeletetaskcaption
msgid "Delete task"
msgstr "Aufgabe löschen"

#: mainwindow.sdeletetaskmessage
#, object-pascal-format
msgid "Do you want to delete the task \"%s\"?"
msgstr "Wollen Sie die Aufgabe \"%s\" löschen?"

#: taboutform.caption
msgid "About"
msgstr "Über"

#: taboutform.closebutton.caption
msgid "Close"
msgstr "Schließen"

#: taboutform.copyrightlabel.caption
msgid "Copyright Herbert Reiter"
msgstr ""

#: taboutform.downloadlinklabel.caption
msgid "https://gitlab.com/moasda/task-organizer"
msgstr ""

#: taboutform.downloadtitlelabel.caption
msgid "Download + Source Code:"
msgstr "Herunterladen + Quellcode:"

#: taboutform.licenselabel.caption
msgid "License: GPL-3.0-only"
msgstr "Lizenz: GPL-3.0-only"

#: taboutform.titlelabel.caption
msgctxt "taboutform.titlelabel.caption"
msgid "Personal Kanban Task Organizer"
msgstr ""

#: taboutform.versionlabel.caption
msgid "Version: {}"
msgstr ""

#: taskeditdlg.sttaskstatuscancelled
msgid "Cancelled"
msgstr "Abgebrochen"

#: taskeditdlg.sttaskstatuscompleted
msgid "Completed"
msgstr "Abgeschlossen"

#: taskeditdlg.sttaskstatusinprocess
msgid "In process"
msgstr "In Arbeit"

#: taskeditdlg.sttaskstatusneedsaction
msgid "Needs action"
msgstr "Erfordert Aktion"

#: taskmodel.skanbangrouptitleblocked
msgid "Blocked"
msgstr "Blockiert"

#: taskmodel.skanbangrouptitledone
msgid "Done"
msgstr "Erledigt"

#: taskmodel.skanbangrouptitleidea
msgid "Ideas"
msgstr "Ideen"

#: taskmodel.skanbangrouptitlemonth
msgid "This month"
msgstr "Diesen Monat"

#: taskmodel.skanbangrouptitlequarter
msgid "This quarter"
msgstr "Dieses Quartal"

#: taskmodel.skanbangrouptitletoday
msgid "Today"
msgstr "Heute"

#: taskmodel.skanbangrouptitleweek
msgid "This week"
msgstr "Diese Woche"

#: taskmodel.skanbangrouptitleyear
msgid "This year"
msgstr "Dieses Jahr"

#: tkanbangroupform.cancelbutton.caption
msgctxt "tkanbangroupform.cancelbutton.caption"
msgid "Cancel"
msgstr "Abbrechen"

#: tkanbangroupform.caption
msgid "Edit Kanban Group"
msgstr "Kanban-Gruppe bearbeiten"

#: tkanbangroupform.closetaskscheckbox.caption
msgid "Close tasks when moved to this group"
msgstr "Schließe Aufgaben wenn sie in diese Gruppe verschoben werden"

#: tkanbangroupform.icalendarexportcheckbox.caption
msgid "Export tasks in iCalendar"
msgstr "Exportiere Aufgaben in iCalendar"

#: tkanbangroupform.okbutton.caption
msgctxt "tkanbangroupform.okbutton.caption"
msgid "OK"
msgstr "OK"

#: tkanbangroupform.titlelabel.caption
msgctxt "tkanbangroupform.titlelabel.caption"
msgid "Title:"
msgstr "Titel:"

#: tmainform.burgerbutton.hint
msgid "Show application menu"
msgstr "Anwendungsmenü anzeigen"

#: tmainform.burgermenuitemabout.caption
msgid "About..."
msgstr "Über..."

#: tmainform.burgermenuitemsettings.caption
msgid "Settings..."
msgstr "Einstellungen..."

#: tmainform.caption
msgctxt "tmainform.caption"
msgid "Personal Kanban Task Organizer"
msgstr ""

#: tmainform.kanbangroupmenuedit.caption
msgid "Edit Kanban Group..."
msgstr "Kanban-Gruppe bearbeiten..."

#: tmainform.kanbangroupmenushowoldtasks.caption
msgid "Show old tasks"
msgstr "Alte Aufgaben anzeigen"

#: tmainform.searchedit.hint
msgid "Search for tasks"
msgstr "Aufgaben suchen"

#: tmainform.taskmenudeleteitem.caption
msgid "Delete task..."
msgstr "Aufgabe löschen..."

#: tmainform.taskmenuedititem.caption
msgid "Edit task..."
msgstr "Aufgabe bearbeiten..."

#: tsettingsform.alertdescriptionlabel.caption
msgid "minutes before the event"
msgstr "Minuten vor der Aufgabe"

#: tsettingsform.alertlabel.caption
msgid "Show alert:"
msgstr "Alarm anzeigen:"

#: tsettingsform.cancelbutton.caption
msgctxt "tsettingsform.cancelbutton.caption"
msgid "Cancel"
msgstr "Abbrechen"

#: tsettingsform.caption
msgid "Settings"
msgstr "Einstellungen"

#: tsettingsform.explanationlabel.caption
msgid "The following texts are initially copied when you create a new task."
msgstr "Nachfolgende Texte werden initial in neue Aufgaben übernommen."

#: tsettingsform.hideoldtaskscheckbox.caption
msgid "Hide old tasks after:"
msgstr "Alte Aufgaben ausblenden nach:"

#: tsettingsform.hideoldtasksdayslabel.caption
msgid "days"
msgstr "Tagen"

#: tsettingsform.icalcheckbox.caption
msgid "iCalendar export enabled"
msgstr "iCalendar-Export aktivieren"

#: tsettingsform.kanbangroupexportlabel.caption
msgid "Kanban Groups to export as iCalendar:"
msgstr "Kanban-Gruppen im iCalendar exportieren:"

#: tsettingsform.languagelabel.caption
msgid "Language:"
msgstr "Sprache:"

#: tsettingsform.minuteslabel.caption
msgid "minutes"
msgstr "Minuten"

#: tsettingsform.okbutton.caption
msgctxt "tsettingsform.okbutton.caption"
msgid "OK"
msgstr "OK"

#: tsettingsform.portlabel.caption
msgid "Server port:"
msgstr "Server-Port:"

#: tsettingsform.tabsheetgeneral.caption
msgid "General"
msgstr "Allgemein"

#: tsettingsform.tabsheeticalendar.caption
msgid "iCalendar"
msgstr "iCalendar"

#: tsettingsform.tabsheettemplate.caption
msgid "Task Template"
msgstr "Aufgabenvorlage"

#: tsettingsform.taskeditorfonttextlabel.caption
msgid "Font name"
msgstr "Schriftartname"

#: tsettingsform.taskeditorlabel.caption
msgid "Task Editor Font:"
msgstr "Aufgabeneditor Schriftart:"

#: tsettingsform.taskeditorselectfontbutton.caption
msgid "Select Font..."
msgstr "Schriftart wählen..."

#: tsettingsform.templatedetailslabel.caption
msgctxt "tsettingsform.templatedetailslabel.caption"
msgid "Details:"
msgstr "Details:"

#: tsettingsform.templatetitlelabel.caption
msgctxt "tsettingsform.templatetitlelabel.caption"
msgid "Title:"
msgstr "Titel:"

#: tsettingsform.timeoffsetlabel.caption
msgid "Time offset to next task:"
msgstr "Zeit zwischen Aufgaben:"

#: tsettingsform.transparencylabel.caption
msgid "Main window transparency:"
msgstr "Transparenz des Hauptfensters:"

#: tsettingsform.urlcopybutton.hint
msgid "Copy to clipboard"
msgstr "In die Zwischenablage kopieren"

#: tsettingsform.urllabel.caption
msgid "Server URL for calendar clients:"
msgstr "Server-URL für Kalender-Anwendungen:"

#: tsettingsform.urllink.caption
msgid "http://localhost:12080/calendar.ics"
msgstr ""

#: tsettingsform.workfolderbutton.caption
msgid "Select Folder..."
msgstr "Ordner wählen..."

#: tsettingsform.workfolderlabel.caption
msgid "Work folder (contains tasks.json file):"
msgstr "Arbeitsordner (enthält Datei tasks.json):"

#: tsettingsform.workfoldervaluelabel.caption
msgid "WorkFolderValueLabel"
msgstr ""

#: ttaskeditform.cancelbutton.caption
msgctxt "ttaskeditform.cancelbutton.caption"
msgid "Cancel"
msgstr "Abbrechen"

#: ttaskeditform.caption
msgid "Edit Task"
msgstr "Aufgabe bearbeiten"

#: ttaskeditform.completeddatelabel.caption
msgid "Completed Date:"
msgstr "Erledigt am:"

#: ttaskeditform.createddatelabel.caption
msgid "Created Date:"
msgstr "Erstellt am:"

#: ttaskeditform.detailslabel.caption
msgctxt "ttaskeditform.detailslabel.caption"
msgid "Details:"
msgstr "Details:"

#: ttaskeditform.duedatelabel.caption
msgid "Due Date:"
msgstr "Fällig am:"

#: ttaskeditform.kanbangrouplabel.caption
msgid "Kanban Group:"
msgstr "Kanban-Gruppe:"

#: ttaskeditform.lastmodifieddatelabel.caption
msgid "Last Modified Date:"
msgstr "Zuletzt geändert am:"

#: ttaskeditform.okbutton.caption
msgctxt "ttaskeditform.okbutton.caption"
msgid "OK"
msgstr "OK"

#: ttaskeditform.savetaskaction.caption
msgid "SaveTask"
msgstr ""

#: ttaskeditform.startdatelabel.caption
msgid "Start Date:"
msgstr "Anfangen am:"

#: ttaskeditform.tabsheet1.caption
msgid "Description"
msgstr "Beschreibung"

#: ttaskeditform.tabsheet2.caption
msgid "Status"
msgstr "Status"

#: ttaskeditform.taskidlabel.caption
msgid "Task ID:"
msgstr "Aufgaben-ID:"

#: ttaskeditform.taskstatuslabel.caption
msgid "Status:"
msgstr "Status:"

#: ttaskeditform.titlelabel.caption
msgctxt "ttaskeditform.titlelabel.caption"
msgid "Title:"
msgstr "Titel:"
