{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit SettingsDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  IniPropStorage, MaskEdit, Spin, ComCtrls, Buttons, CheckLst;

type
  TSettingsForm = class(TForm)
    AlertDescriptionLabel: TLabel;
    AlertEdit: TSpinEdit;
    AlertLabel: TLabel;
    WorkFolderButton: TButton;
    ExplanationLabel: TLabel;
    SelectDirectoryDialog: TSelectDirectoryDialog;
    WorkFolderValueLabel: TLabel;
    WorkFolderPanel: TPanel;
    WorkFolderLabel: TLabel;
    TemplateDetailsLabel: TLabel;
    TemplateDetailsEdit: TMemo;
    TemplateTitleEdit: TEdit;
    KanbanGroupExportCheckListBox: TCheckListBox;
    HideOldTasksCheckBox: TCheckBox;
    HideOldTasksDaysLabel: TLabel;
    HideOldTasksEdit: TSpinEdit;
    HideOldTasksPanel: TPanel;
    KanbanGroupExportLabel: TLabel;
    TemplateTitleLabel: TLabel;
    TabSheetTemplate: TTabSheet;
    TransparencyLabel: TLabel;
    MinutesLabel: TLabel;
    AlertPanel: TPanel;
    TransparencyTrackBar: TTrackBar;
    UrlCopyButton: TSpeedButton;
    UrlLink: TLabel;
    UrlPanel: TPanel;
    TimeOffsetEdit: TSpinEdit;
    TimeOffsetPanel: TPanel;
    PortEdit: TSpinEdit;
    ServerPortPanel: TPanel;
    TaskEditorFontTextLabel: TLabel;
    IcalCheckBox: TCheckBox;
    IniPropStorage: TIniPropStorage;
    LanguageLabel: TLabel;
    TaskEditorLabel: TLabel;
    LanguageComboBox: TComboBox;
    PageControl1: TPageControl;
    TaskEditorPanel: TPanel;
    PortLabel: TLabel;
    TaskEditorSelectFontButton: TButton;
    TabSheetGeneral: TTabSheet;
    TabSheetICalendar: TTabSheet;
    FontDialog: TFontDialog;
    OkButton: TButton;
    CancelButton: TButton;
    ButtonPanel: TPanel;
    TimeOffsetLabel: TLabel;
    UrlLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HideOldTasksCheckBoxChange(Sender: TObject);
    procedure IcalCheckBoxChange(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure PortEditChange(Sender: TObject);
    procedure TaskEditorSelectFontButtonClick(Sender: TObject);
    procedure TransparencyTrackBarChange(Sender: TObject);
    procedure UrlCopyButtonClick(Sender: TObject);
    procedure WorkFolderButtonClick(Sender: TObject);
  private
    procedure LoadSettings;
    procedure SaveSettings;
    procedure UpdateFontPreview;
  public

  end;

var
  SettingsForm: TSettingsForm;

implementation

uses LCLTranslator, Translations, LCLIntf, Clipbrd, TaskEditDlg,
  HttpCommunication, Constants, MainWindow, TaskModel, TextUtils;

{$R *.lfm}

procedure TSettingsForm.FormCreate(Sender: TObject);
begin
  IniPropStorage.IniFileName := MainForm.IniPropStorage.IniFileName;
end;

procedure TSettingsForm.FormShow(Sender: TObject);
begin
  LoadSettings;
  PageControl1.PageIndex := 0;
  IcalCheckBoxChange(self);
end;

procedure TSettingsForm.TaskEditorSelectFontButtonClick(Sender: TObject);
begin
  if FontDialog.Execute then
  begin
    UpdateFontPreview;
  end;
end;

procedure TSettingsForm.HideOldTasksCheckBoxChange(Sender: TObject);
begin
  HideOldTasksEdit.Enabled := HideOldTasksCheckBox.Checked;
  HideOldTasksDaysLabel.Enabled := HideOldTasksCheckBox.Checked;
end;

procedure TSettingsForm.TransparencyTrackBarChange(Sender: TObject);
begin
  MainForm.AlphaBlendValue := TransparencyTrackBar.Position;
end;

procedure TSettingsForm.UrlCopyButtonClick(Sender: TObject);
begin
  Clipboard.AsText := UrlLink.Caption;
end;

procedure TSettingsForm.WorkFolderButtonClick(Sender: TObject);
begin
  SelectDirectoryDialog.FileName := WorkFolderValueLabel.Caption;
  if SelectDirectoryDialog.Execute then
  begin
    WorkFolderValueLabel.Caption := SelectDirectoryDialog.FileName;
  end;
end;

procedure TSettingsForm.UpdateFontPreview;
var FontText: String;
begin
  TaskEditorFontTextLabel.Font.Name := FontDialog.Font.Name;
  TaskEditorFontTextLabel.Font.Size := FontDialog.Font.Size;
  FontText := TaskEditorFontTextLabel.Font.Name;
  if TaskEditorFontTextLabel.Font.Size > 0 then
  begin
    FontText := FontText + ' (' + IntToStr(TaskEditorFontTextLabel.Font.Size) + ' pt)';
  end;
  TaskEditorFontTextLabel.Caption := FontText;
end;

procedure TSettingsForm.IcalCheckBoxChange(Sender: TObject);
var Status: Boolean;
begin
  Status := IcalCheckBox.Checked;
  PortLabel.Enabled := Status;
  PortEdit.Enabled := Status;
  TimeOffsetLabel.Enabled := Status;
  TimeOffsetEdit.Enabled := Status;
  MinutesLabel.Enabled := Status;
  AlertLabel.Enabled := Status;
  AlertEdit.Enabled := Status;
  AlertDescriptionLabel.Enabled := Status;
  KanbanGroupExportLabel.Enabled := Status;
  KanbanGroupExportCheckListBox.Enabled := Status;
  UrlLabel.Enabled := Status;
  UrlLink.Enabled := Status;
  PortEditChange(Sender);
end;

procedure TSettingsForm.PortEditChange(Sender: TObject);
begin
  UrlLink.Caption := 'http://localhost:' + IntToStr(PortEdit.Value) + CalendarUrlPath;
end;

procedure TSettingsForm.OkButtonClick(Sender: TObject);
begin
  SaveSettings;
end;

procedure TSettingsForm.LoadSettings;
var Language: String;
    FontName: String;
    FontSize: Integer;
    KanbanGroup: TKanbanGroup;
begin
  // UI language
  Language := IniPropStorage.ReadString(SettingLanguage, '');
  if Language = '' then
  begin
    Language := GetLanguageID.LanguageID;
    WriteLog('Using default language: ' + Language);
  end;
  case Language of
    'de': LanguageComboBox.ItemIndex := 0;
    'en': LanguageComboBox.ItemIndex := 1;
    'nl': LanguageComboBox.ItemIndex := 2;
  else
    LanguageComboBox.ItemIndex := 1; // English
  end;

  // Work folder
  WorkFolderValueLabel.Caption := IniPropStorage.ReadString(SettingWorkFolder, GetUserDir);

  // Task edit font
  FontName := IniPropStorage.ReadString(SettingFontName, '');
  FontSize := IniPropStorage.ReadInteger(SettingFontSize, 0);
  if (FontName <> '') and (FontSize > 0) then
  begin
    FontDialog.Font.Name := FontName;
    FontDialog.Font.Size := FontSize;
  end
  else
  begin
    FontDialog.Font.Name := TaskEditForm.DetailsEdit.Font.Name;
    FontDialog.Font.Size := TaskEditForm.DetailsEdit.Font.Size;
  end;
  UpdateFontPreview;

  // Hide done tasks after x days
  HideOldTasksCheckBox.Checked := not IniPropStorage.ReadBoolean(SettingShowOldTasksEnabled, SettingShowOldTasksEnabledDefault);
  HideOldTasksEdit.Value := IniPropStorage.ReadInteger(SettingShowOldTasksDays, SettingShowOldTasksDaysDefault);
  HideOldTasksCheckBoxChange(nil);

  // Main window transparency
  TransparencyTrackBar.Position := IniPropStorage.ReadInteger(SettingTransparency, SettingTransparencyDefault);

  // Task template
  TemplateTitleEdit.Text := IniPropStorage.ReadString(SettingTemplateTitle, '');
  TemplateDetailsEdit.Text := DecodeLineBreaks(IniPropStorage.ReadString(SettingTemplateDetails, ''));

  // iCalendar
  IcalCheckBox.Checked := IniPropStorage.ReadBoolean(SettingIcalEnabled, SettingIcalEnabledDefault);
  PortEdit.Value := IniPropStorage.ReadInteger(SettingIcalPort, SettingIcalPortDefault);
  TimeOffsetEdit.Value := IniPropStorage.ReadInteger(SettingIcalTimeOffset, SettingIcalTimeOffsetDefault);
  AlertEdit.Value := IniPropStorage.ReadInteger(SettingIcalAlert, SettingIcalAlertDefault);
  KanbanGroupExportCheckListBox.Clear;
  for KanbanGroup in MainForm.TaskManager.GetKanbanGroups do
  begin
    KanbanGroupExportCheckListBox.AddItem(KanbanGroup.Title, nil);
    KanbanGroupExportCheckListBox.Checked[KanbanGroup.Index] := KanbanGroup.ICalendarExport;
  end;
end;

procedure TSettingsForm.SaveSettings;
var Language: String;
    KanbanGroup: TKanbanGroup;
begin
  // Work folder
  IniPropStorage.WriteString(SettingWorkFolder, WorkFolderValueLabel.Caption);

  // Task edit font
  if (FontDialog.Font.Name <> '') and (FontDialog.Font.Size > 0) then
  begin
    IniPropStorage.WriteString(SettingFontName, FontDialog.Font.Name);
    IniPropStorage.WriteInteger(SettingFontSize, FontDialog.Font.Size);
    TaskEditForm.DetailsEdit.Font.Name := FontDialog.Font.Name;
    TaskEditForm.DetailsEdit.Font.Size := FontDialog.Font.Size;
  end;

  // Hide done tasks after x days
  IniPropStorage.WriteBoolean(SettingShowOldTasksEnabled, not HideOldTasksCheckBox.Checked);
  IniPropStorage.WriteInteger(SettingShowOldTasksDays, HideOldTasksEdit.Value);

  // Main window transparency
  IniPropStorage.WriteInteger(SettingTransparency, TransparencyTrackBar.Position);

  // Task template
  IniPropStorage.WriteString(SettingTemplateTitle, TemplateTitleEdit.Text);
  IniPropStorage.WriteString(SettingTemplateDetails, EncodeLineBreaks(TemplateDetailsEdit.Text));

  // iCalendar
  IniPropStorage.WriteBoolean(SettingIcalEnabled, IcalCheckBox.Checked);
  IniPropStorage.WriteInteger(SettingIcalPort, PortEdit.Value);
  IniPropStorage.WriteInteger(SettingIcalTimeOffset, TimeOffsetEdit.Value);
  IniPropStorage.WriteInteger(SettingIcalAlert, AlertEdit.Value);
  for KanbanGroup in MainForm.TaskManager.GetKanbanGroups do
  begin
    KanbanGroup.ICalendarExport := KanbanGroupExportCheckListBox.Checked[KanbanGroup.Index];
  end;

  // UI language
  case LanguageComboBox.ItemIndex of
    0: Language := 'de';
    1: Language := 'en';
    2: Language := 'nl';
  else
    Language := 'en';
  end;
  IniPropStorage.WriteString(SettingLanguage, Language);
  // Call SetDefaultLang after all settings are saved, because this might reset caption values
  SetDefaultLang(Language);
end;

end.

