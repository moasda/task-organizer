{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit TaskModel;

{$mode objfpc}{$H+}

interface

uses Classes, SysUtils, Fgl, JsonTools;

type
  TTaskStatus = (NeedsAction, InProcess, Completed, Cancelled);

  TKanbanGroup = class
  public
    Index: Integer;
    Title: String;  // '' -> default title
    CloseTasks: Boolean;
    ICalendarExport: Boolean;
  end;

  TTask = class
  public
    Id: Integer;
    Title: String;
    Details: String;
    CreatedDate: TDateTime;      // 0 = empty
    LastModifiedDate: TDateTime; // 0 = empty
    StartDate: TDateTime;        // 0 = empty
    DueDate: TDateTime;          // 0 = empty
    CompletedDate: TDateTime;    // 0 = empty
    Status: TTaskStatus;
    KanbanGroupIndex: Integer;

    procedure UpdateTask(Task: TTask);
  end;

  TKanbanGroupList = specialize TFPGList<TKanbanGroup>;
  TTaskList = specialize TFPGList<TTask>;

  TTaskManager = class
  private
    KanbanGroups: TKanbanGroupList;
    Tasks: TTaskList;
  public
    constructor Create;
    destructor Destroy; override;
    function GetKanbanGroups: TKanbanGroupList;
    function AddTask(Task: TTask): Integer;
    procedure DeleteTask(Id: Integer);
    function GetTask(Id: Integer): TTask;
    function GetTasks: TTaskList;
    function GetICalendarTasks: TTaskList;
    procedure ReadFromFile(Filename: String);
    procedure WriteToFile(Filename: String);
  private
    procedure Clear;
    procedure ReadVersion10(Json: TJsonNode);
    procedure ReadVersion20(Json: TJsonNode);
    procedure ReadVersion21(Json: TJsonNode);
    function WriteVersion21: TJsonNode;
    procedure AddKanbanGroup(Index: Integer; CloseTasks: Boolean; ICalendarExport: Boolean);
    procedure AddStandardKanbanGroups;
  end;

function ContainsText(Text: String; Task: TTask): Boolean;
function CompareTitle(const Task1: TTask; const Task2: TTask): Integer;
function GetDefaultGroupTitle(Index: Integer): String;

implementation

uses LCLTranslator, Constants, TypInfo, TextUtils;

type
  TKanbanGroupLegacy = (Idea, ThisYear, ThisQuarter, ThisMonth, ThisWeek, Today, Blocked, Done);

resourcestring
  SKanbanGroupTitleIdea    = 'Ideas';
  SKanbanGroupTitleYear    = 'This year';
  SKanbanGroupTitleQuarter = 'This quarter';
  SKanbanGroupTitleMonth   = 'This month';
  SKanbanGroupTitleWeek    = 'This week';
  SKanbanGroupTitleToday   = 'Today';
  SKanbanGroupTitleBlocked = 'Blocked';
  SKanbanGroupTitleDone    = 'Done';

const
  TasksFileVersion10 = '1.0';
  TasksFileVersion20 = '2.0';
  TasksFileVersion21 = '2.1';
  SKanbanGroupTitles: array[0..7] of PString
                      = (@SKanbanGroupTitleIdea, @SKanbanGroupTitleYear, @SKanbanGroupTitleQuarter,
                         @SKanbanGroupTitleMonth, @SKanbanGroupTitleWeek, @SKanbanGroupTitleToday,
                         @SKanbanGroupTitleBlocked, @SKanbanGroupTitleDone);

procedure TTask.UpdateTask(Task: TTask);
begin
  Title := Task.Title;
  Details := Task.Details;
  LastModifiedDate := Task.LastModifiedDate;
  StartDate := Task.StartDate;
  DueDate := Task.DueDate;
  CompletedDate := Task.CompletedDate;
  Status := Task.Status;
  KanbanGroupIndex := Task.KanbanGroupIndex;
end;

constructor TTaskManager.Create;
begin
  inherited;
  KanbanGroups := TKanbanGroupList.Create;
  Tasks := TTaskList.Create;
  AddStandardKanbanGroups;
end;

destructor TTaskManager.Destroy;
begin
  Clear;
  Tasks.Free;
  KanbanGroups.Free;
  inherited;
end;

procedure TTaskManager.AddKanbanGroup(Index: Integer; CloseTasks: Boolean; ICalendarExport: Boolean);
var KanbanGroup: TKanbanGroup;
begin
  KanbanGroup := TKanbanGroup.Create;
  KanbanGroup.Index := Index;
  KanbanGroup.CloseTasks := CloseTasks;
  KanbanGroup.ICalendarExport := ICalendarExport;
  KanbanGroups.Add(KanbanGroup);
end;

procedure TTaskManager.AddStandardKanbanGroups;
begin
  AddKanbanGroup(0, false, false);
  AddKanbanGroup(1, false, false);
  AddKanbanGroup(2, false, false);
  AddKanbanGroup(3, false, false);
  AddKanbanGroup(4, false, false);
  AddKanbanGroup(5, false, true);
  AddKanbanGroup(6, false, false);
  AddKanbanGroup(7, true,  false);
end;

function TTaskManager.GetKanbanGroups: TKanbanGroupList;
begin
  Result := KanbanGroups;
end;

function TTaskManager.AddTask(Task: TTask): Integer;
begin
  // Generate unique id
  if Task.Id = 0 then
  begin
    repeat
      Task.Id := Random(MaxInt);
    until GetTask(Task.Id) = nil;
    Task.CreatedDate := Now;
    Task.LastModifiedDate := Task.CreatedDate;
  end;
  Tasks.Add(Task);
  Result := Task.Id;
end;

procedure TTaskManager.DeleteTask(Id: Integer);
var I: Integer;
begin
  for I := 0 to Tasks.Count - 1 do
  begin
    if Tasks[I].Id = Id then
    begin
      Tasks.Delete(I);
      Break;
    end;
  end;
end;

// retuls = nil -> element not found
function TTaskManager.GetTask(Id: Integer): TTask;
var I: Integer;
begin
  Result := nil;
  for I := 0 to Tasks.Count - 1 do
  begin
    if Tasks[I].Id = Id then
    begin
      Result := Tasks[I];
      Break;
    end;
  end;
end;

function TTaskManager.GetTasks: TTaskList;
begin
  Result := Tasks;
end;

function TTaskManager.GetICalendarTasks: TTaskList;
var Task: TTask;
    KanbanGroup: TKanbanGroup;
begin
  Result := TTaskList.Create;
  for Task in Tasks do
  begin
    KanbanGroup := KanbanGroups[Task.KanbanGroupIndex];
    if KanbanGroup.ICalendarExport then
    begin
      Result.Add(Task);
    end;
  end;
  Result.Sort(@CompareTitle);
end;

procedure TTaskManager.ReadFromFile(Filename: String);
var Json: TJsonNode;
    Version: String;
begin
  WriteLog('Reading tasks file: ' + Filename);
  Json := TJsonNode.Create;
  try
    Json.LoadFromFile(Filename);
    Version := Json.Find('version').AsString;
    if Version = TasksFileVersion10 then
    begin
      ReadVersion10(Json);
    end
    else
    if Version = TasksFileVersion20 then
    begin
      ReadVersion20(Json);
    end
    else
    if Version = TasksFileVersion21 then
    begin
      ReadVersion21(Json);
    end
    else
    begin
      raise Exception.Create('Unsupported tasks file version: ' + Version);
    end;
  finally
    Json.Free;
  end;
end;

procedure TTaskManager.Clear;
var Task: TTask;
    KanbanGroup: TKanbanGroup;
begin
  for Task in Tasks do
  begin
    Task.Free;
  end;
  Tasks.Clear;
  for KanbanGroup in KanbanGroups do
  begin
    KanbanGroup.Free;
  end;
  KanbanGroups.Clear;
end;

procedure TTaskManager.ReadVersion10(Json: TJsonNode);
var TaskNodes, TaskNode: TJsonNode;
    Task: TTask;
begin
  TaskNodes := Json.Find('tasks');
  for TaskNode in TaskNodes do
  begin
    Task := TTask.Create;
    Task.Id := 0; // will be set later
    Task.Title := TaskNode.Find('title').AsString;
    Task.Details := TaskNode.Find('details').AsString;
    Task.CreatedDate := Now;
    Task.LastModifiedDate := Task.CreatedDate;
    Task.KanbanGroupIndex := Round(TaskNode.Find('group').AsNumber);
    case Task.KanbanGroupIndex of
      6: Task.Status := InProcess;
      7: Task.Status := Completed;
      else Task.Status := NeedsAction;
    end;
    AddTask(Task);
  end;
  WriteLog(IntToStr(TaskNodes.Count) + ' tasks read');
end;

function FindAttributeString(Node: TJsonNode; AttributeName: String): String;
var AttributeNode: TJsonNode;
begin
  AttributeNode := Node.Find(AttributeName);
  if AttributeNode <> nil then
  begin
    Result := AttributeNode.AsString;
  end
  else
  begin
    Result := '';
  end;
end;

function FindAttributeInteger(Node: TJsonNode; AttributeName: String): Integer;
var AttributeNode: TJsonNode;
begin
  AttributeNode := Node.Find(AttributeName);
  if AttributeNode <> nil then
  begin
    Result := Round(AttributeNode.AsNumber);
  end
  else
  begin
    Result := 0;
  end;
end;

function FindAttributeBoolean(Node: TJsonNode; AttributeName: String): Boolean;
var AttributeNode: TJsonNode;
begin
  AttributeNode := Node.Find(AttributeName);
  if AttributeNode <> nil then
  begin
    Result := AttributeNode.AsBoolean;
  end
  else
  begin
    Result := False;
  end;
end;

function FindAttributeDateTime(Node: TJsonNode; AttributeName: String): TDateTime;
begin
  Result := String2DateTime(FindAttributeString(Node, AttributeName));
end;

procedure TTaskManager.ReadVersion20(Json: TJsonNode);
var TaskNodes, TaskNode: TJsonNode;
    Task: TTask;
    KanbanGroupLegacy: TKanbanGroupLegacy;
begin
  TaskNodes := Json.Find('tasks');
  for TaskNode in TaskNodes do
  begin
    Task := TTask.Create;
    Task.Id := FindAttributeInteger(TaskNode, 'id');
    Task.Title := FindAttributeString(TaskNode, 'title');
    Task.Details := FindAttributeString(TaskNode, 'details');
    Task.CreatedDate := FindAttributeDateTime(TaskNode, 'created-date');
    Task.LastModifiedDate := FindAttributeDateTime(TaskNode, 'last-modified-date');
    Task.StartDate := FindAttributeDateTime(TaskNode, 'start-date');
    Task.DueDate := FindAttributeDateTime(TaskNode, 'due-date');
    Task.CompletedDate := FindAttributeDateTime(TaskNode, 'completed-date');
    Task.Status := TTaskStatus(GetEnumValue(TypeInfo(TTaskStatus), FindAttributeString(TaskNode, 'status')));
    KanbanGroupLegacy := TKanbanGroupLegacy(GetEnumValue(TypeInfo(TKanbanGroupLegacy), FindAttributeString(TaskNode, 'kanban-group')));
    Task.KanbanGroupIndex := Ord(KanbanGroupLegacy);
    AddTask(Task);
  end;
  WriteLog(IntToStr(TaskNodes.Count) + ' tasks read');
end;

procedure TTaskManager.ReadVersion21(Json: TJsonNode);
var KanbanGroupNodes, KanbanGroupNode: TJsonNode;
    KanbanGroup: TKanbanGroup;
    TaskNodes, TaskNode: TJsonNode;
    Task: TTask;
begin
  Clear;
  KanbanGroupNodes := Json.Find('kanban-groups');
  for KanbanGroupNode in KanbanGroupNodes do
  begin
    KanbanGroup := TKanbanGroup.Create;
    KanbanGroup.Index := FindAttributeInteger(KanbanGroupNode, 'index');
    if KanbanGroup.Index <> KanbanGroups.Count then
    begin
      WriteLog('Unexpected kanban group index ' + IntToStr(KanbanGroup.Index)
        + ' at position ' + IntToStr(KanbanGroups.Count));
      Continue;
    end;
    KanbanGroup.Title := FindAttributeString(KanbanGroupNode, 'title');
    KanbanGroup.CloseTasks := FindAttributeBoolean(KanbanGroupNode, 'close-tasks');
    KanbanGroup.ICalendarExport := FindAttributeBoolean(KanbanGroupNode, 'icalendar-export');
    KanbanGroups.Add(KanbanGroup);
  end;
  TaskNodes := Json.Find('tasks');
  for TaskNode in TaskNodes do
  begin
    Task := TTask.Create;
    Task.Id := FindAttributeInteger(TaskNode, 'id');
    Task.Title := FindAttributeString(TaskNode, 'title');
    Task.Details := FindAttributeString(TaskNode, 'details');
    Task.CreatedDate := FindAttributeDateTime(TaskNode, 'created-date');
    Task.LastModifiedDate := FindAttributeDateTime(TaskNode, 'last-modified-date');
    Task.StartDate := FindAttributeDateTime(TaskNode, 'start-date');
    Task.DueDate := FindAttributeDateTime(TaskNode, 'due-date');
    Task.CompletedDate := FindAttributeDateTime(TaskNode, 'completed-date');
    Task.Status := TTaskStatus(GetEnumValue(TypeInfo(TTaskStatus), FindAttributeString(TaskNode, 'status')));
    Task.KanbanGroupIndex := FindAttributeInteger(TaskNode, 'kanban-group-index');
    AddTask(Task);
  end;
  WriteLog(IntToStr(TaskNodes.Count) + ' tasks read');
end;

procedure AddNodeAttribute(Node: TJsonNode; AttributeName: String; DateTime: TDateTime);
begin
  if DateTime > 0 then
  begin
    Node.Add(AttributeName, DateTime2String(DateTime));
  end;
end;

procedure TTaskManager.WriteToFile(Filename: String);
var Json: TJsonNode;
begin
  WriteLog('Writing tasks file: ' + Filename);
  Json := WriteVersion21;
  try
    Json.SaveToFile(Filename);
    WriteLog(IntToStr(Tasks.Count) + ' tasks written');
  finally
    Json.Free;
  end;
end;

function TTaskManager.WriteVersion21: TJsonNode;
var KanbanGroupNodes, KanbanGroupNode: TJsonNode;
    TaskNodes, TaskNode: TJsonNode;
    KanbanGroup: TKanbanGroup;
    Task: TTask;
begin
  Result := TJsonNode.Create;
  Result.Add('description', 'Personal Kanban Tasks File');
  Result.Add('version', TasksFileVersion21);

  KanbanGroupNodes := Result.Add('kanban-groups', nkArray);
  for KanbanGroup in KanbanGroups do
  begin
    KanbanGroupNode := KanbanGroupNodes.Add('');
    KanbanGroupNode.Add('index', KanbanGroup.Index);
    KanbanGroupNode.Add('title', KanbanGroup.Title);
    KanbanGroupNode.Add('close-tasks', KanbanGroup.CloseTasks);
    KanbanGroupNode.Add('icalendar-export', KanbanGroup.ICalendarExport);
  end;

  TaskNodes := Result.Add('tasks', nkArray);
  for Task in Tasks do
  begin
    TaskNode := TaskNodes.Add('');
    TaskNode.Add('id', Task.Id);
    TaskNode.Add('title', Task.Title);
    TaskNode.Add('details', Task.Details);
    AddNodeAttribute(TaskNode, 'created-date', Task.CreatedDate);
    AddNodeAttribute(TaskNode, 'last-modified-date', Task.LastModifiedDate);
    AddNodeAttribute(TaskNode, 'start-date', Task.StartDate);
    AddNodeAttribute(TaskNode, 'due-date', Task.DueDate);
    AddNodeAttribute(TaskNode, 'completed-date', Task.CompletedDate);
    TaskNode.Add('status', GetEnumName(TypeInfo(TTaskStatus), Ord(Task.Status)));
    TaskNode.Add('kanban-group-index', Task.KanbanGroupIndex);
  end;
end;

// Split search string into words and search for all words.
function ContainsText(Text: String; Task: TTask): Boolean;
var Words: TStringArray;
    I: Integer;
begin
  Text := AnsiUpperCase(Text).Trim();
  Words := Text.Split(' ', TStringSplitOptions.ExcludeEmpty);
  Result := True;
  for I := 0 to Length(Words) - 1 do
  begin
    if not AnsiUpperCase(Task.Title).Contains(Words[I]) and
       not AnsiUpperCase(Task.Details).Contains(Words[I]) then
    begin
      Result := False;
      Break;
    end;
  end;
end;

function CompareTitle(const Task1: TTask; const Task2: TTask): Integer;
begin
  Result := AnsiCompareText(Task1.Title, Task2.Title);
end;

function GetDefaultGroupTitle(Index: Integer): String;
begin
  if ((Index >= 0) and (Index < Length(SKanbanGroupTitles))) then
    Result := Format(SKanbanGroupTitles[Index]^, [])
  else
    Result := '';
end;

end.

