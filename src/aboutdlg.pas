{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit AboutDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

type

  { TAboutForm }

  TAboutForm = class(TForm)
    BackgroundPanel: TPanel;
    CloseButton: TButton;
    CopyrightLabel: TLabel;
    DownloadLinkLabel: TLabel;
    DownloadTitleLabel: TLabel;
    Image1: TImage;
    LicenseLabel: TLabel;
    ButtonPanel: TPanel;
    TitleLabel: TLabel;
    VersionLabel: TLabel;
    procedure DownloadLinkLabelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  AboutForm: TAboutForm;

implementation

uses LCLTranslator, LCLIntf, Constants;

{$R *.lfm}

{ TAboutForm }

procedure TAboutForm.FormShow(Sender: TObject);
var Version: String;
begin
  Version := GetProgramVersion;
  VersionLabel.Caption := StringReplace(VersionLabel.Caption, '{}', Version, [rfReplaceAll]);
end;

procedure TAboutForm.DownloadLinkLabelClick(Sender: TObject);
begin
  OpenURL(DownloadLinkLabel.Caption);
end;

end.

