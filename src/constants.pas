{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit Constants;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

const
  ProgramName = 'Personal Kanban Task Organizer';
  TasksFilename = 'tasks.json';
  SettingsFilename = 'personal-kanban.ini';

  SettingLanguage = 'Language'; // ISO code, e.g. 'en'
  SettingWorkFolder = 'WorkFolder';
  SettingFontName = 'FontName';
  SettingFontSize = 'FontSize'; // integer, pt
  SettingShowOldTasksEnabled = 'ShowOldTasksEnabled'; // boolean
  SettingShowOldTasksEnabledDefault = True;
  SettingShowOldTasksDays = 'ShowOldTasksDays'; // integer, days
  SettingShowOldTasksDaysDefault = 7;
  SettingTransparency = 'Transparency';
  SettingTransparencyDefault = 220;
  SettingTemplateTitle = 'TemplateTitle';
  SettingTemplateDetails = 'TemplateDetails';
  SettingIcalEnabled = 'IcalEnabled'; // boolean
  SettingIcalEnabledDefault = False;
  SettingIcalPort = 'IcalPort'; // integer 1024..65535
  SettingIcalPortDefault = 12080;
  SettingIcalTimeOffset = 'IcalTimeOffset'; // integer, minutes
  SettingIcalTimeOffsetDefault = 15;
  SettingIcalAlert = 'IcalAlert'; // integer, minutes
  SettingIcalAlertDefault = 0;

  // Base color: #58DB84
  // https://paletton.com/#uid=72W0u0kj9K78sWNemQUnjCts4vM
  clBlueLight             = TColor($F2E3B5); // #B5E3F2
  clBlueLightMedium       = TColor($E1C980); // #80C9E1
  clBlueMedium            = TColor($CDAF57); // #57AFCD
  clBlueMediumDark        = TColor($B69636); // #3696B6
  clBlueDark              = TColor($9E7D19); // #197D9E
  clGreenLight            = TColor($CBF6B5); // #B5F6CB
  clGreenLightMedium      = TColor($A4E981); // #81E9A4
  clGreenMedium           = TColor($84DB58); // #58DB84
  clGreenMediumDark       = TColor($69CB37); // #37CB69
  clGreenDark             = TColor($4EB917); // #17B94E
  clRedLight              = TColor($BCC6FF); // #FFC6BC
  clRedLightMedium        = TColor($8D9EFF); // #FF9E8D
  clRedMedium             = TColor($667EFF); // #FF7E66
  clRedMediumDark         = TColor($4562FF); // #FF6245
  clRedDark               = TColor($1F42FD); // #FD421F
  clYellowLight           = TColor($BCDEFF); // #FFDEBC
  clYellowLightMedium     = TColor($8DC7FF); // #FFC78D
  clYellowMedium          = TColor($66B5FF); // #FFB566
  clYellowMediumDark      = TColor($45A5FF); // #FFA545
  clYellowDark            = TColor($1F91FD); // #FD911F

  // Unicode characters
  uClock = '⏱'; // U+23F1 (9201) STOPWATCH
  uNumbers: array of String
            = ('⓪', '①', '②', '③', '④', '⑤', '⑥', '⑦', '⑧', '⑨',
               '⑩', '⑪', '⑫', '⑬', '⑭', '⑮', '⑯', '⑰', '⑱', '⑲', '⑳');

function GetProgramVersion: String;
procedure WriteLog(S: String);

implementation

uses
  {$ifdef MSWINDOWS}
    Windows,
  {$endif}
  LazLoggerBase, VersionResource, TextUtils;

{
  Returns the program version in format "x.y.z".
}
function GetProgramVersion: String;
var Info: TVersionResource;
    Stream: TResourceStream;
begin
  Result := '';
  Info := TVersionResource.Create;
  Stream := TResourceStream.CreateFromID(HInstance, 1, PChar(RT_VERSION));
  try
    // Load version data
    Info.SetCustomRawDataStream(Stream);
    Info.FixedInfo;
    Info.SetCustomRawDataStream(nil);
    // Show version number
    Result := Format('%d.%d.%d', [Info.FixedInfo.FileVersion[0], Info.FixedInfo.FileVersion[1], Info.FixedInfo.FileVersion[2]]);
  finally
    Stream.Free;
    Info.Free;
  end;
end;

{
  Write a log line with the current timestamp.
}
procedure WriteLog(S: String);
var TimestampStr: String;
begin
  TimestampStr := DateTime2String(Now);
  DebugLn(TimestampStr + ' ' + S);
end;

end.

