{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit TaskCalendar;

{
  Provides iCalendar support (see https://tools.ietf.org/html/rfc5545)
  to import tasks in calendar applications.
}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DateUtils, LazSysUtils, taskmodel;

const CRLF = #13#10;

type
  TCalendarSettings = record
    TimeOffsetPeriod: Integer; // minutes
    Alert: Integer; // minutes
  end;

function DescriptionFolding(Description: String): String;
function CalculateBaseDateTime(CurrentDateTime: TDateTime): TDateTime;
function CalculateTaskTime(BaseDateTime: TDateTime; TimeOffsetPeriod: Integer;
         IsStart: Boolean; TaskIndex: Integer): TDateTime;
function FormatLocalTime(DateTime: TDateTime): String;
function Tasks2iCalendar(Tasks: TTaskList; Settings: TCalendarSettings): String;

implementation

function CalendarHeader(): String;
begin
  Result := 'BEGIN:VCALENDAR' + CRLF;
  Result := Result + 'VERSION:2.0' + CRLF;
  Result := Result + 'PRODID:Kanban-Tool' + CRLF;
  Result := Result + 'CALSCALE:GREGORIAN' + CRLF;
end;

function CalendarTrailer(): String;
begin
  Result := 'END:VCALENDAR' + CRLF;
end;

{
  Folds the description text as specified in
  https://tools.ietf.org/html/rfc5545#section-3.1
}
function DescriptionFolding(Description: String): String;
const MaxLineLength = 75;
var BeginPos, EndPos: Integer;
begin
  // escape special characters
  Description := Description.Replace('\', '\\');
  Description := Description.Replace(CRLF, '\n');
  Description := Description.Replace(#13, '\n');
  Description := Description.Replace(#10, '\n');
  // folding to multiple lines
  Result := '';
  BeginPos := 0;
  while BeginPos < Description.Length do
  begin
    if Description.Length - BeginPos <= MaxLineLength then
    begin
      // remaining text fits into one line
      EndPos := Description.Length - 1;
    end
    else
    begin
      // try to add a line break after a space character if any
      EndPos := Description.LastIndexOf(' ', BeginPos + MaxLineLength - 1);
      if EndPos < BeginPos then
      begin
        // no space existing, cut after MaxLineLength characters
        EndPos := BeginPos + MaxLineLength - 1
      end;
    end;
    if not Result.IsEmpty then
      Result := Result + CRLF + #9;
    Result := Result + Description.Substring(BeginPos, EndPos - BeginPos + 1);
    BeginPos := EndPos + 1;
  end;
end;

{
  Calculates the start time of the first task in UTC.
  That's the beginning of the current hour.
}
function CalculateBaseDateTime(CurrentDateTime: TDateTime): TDateTime;
var Hour, Minute, Second, MilliSecond: Word;
    CurrentHour: TDateTime;
begin
  DecodeTime(CurrentDateTime, Hour, Minute, Second, MilliSecond);
  CurrentHour := EncodeTime(Hour, 0, 0, 0);
  Result := ComposeDateTime(CurrentDateTime, CurrentHour);
end;

{
  Calculates the start / end time for a task.

  BaseDateTime: Start time of the first task in UTC.
  TimeOffsetPeriod: Minutes between the start times of two tasks.
  IsStart: True -> calculate start time; False -> calculate end time.
  TaskIndex: 0-based unique index of the task.
}
function CalculateTaskTime(BaseDateTime: TDateTime; TimeOffsetPeriod: Integer;
         IsStart: Boolean; TaskIndex: Integer): TDateTime;
begin
  Result := BaseDateTime;
  Result := IncMinute(Result, TaskIndex * TimeOffsetPeriod);
  if not IsStart then
  begin
    Result := IncMinute(Result, TimeOffsetPeriod);
  end;
end;

{
  Formats the given time in the format 'YYYYMMDD"T"HHMMSS"Z"' (UTC).
}
function FormatLocalTime(DateTime: TDateTime): String;
begin
  Result := FormatDateTime('YYYYMMDD"T"HHMMSS"Z"', DateTime);
end;

function Task2Appointment(Task: TTask; BaseDateTime: TDateTime; TaskIndex: Integer; Settings: TCalendarSettings): String;
begin
  Result := 'BEGIN:VEVENT' + CRLF;
  Result := Result + 'UID:' + IntToStr(Task.Id) + CRLF;
  Result := Result + 'SUMMARY:' + Task.Title + CRLF;
  Result := Result + DescriptionFolding('DESCRIPTION:' + Task.Details) + CRLF;
  Result := Result + 'DTSTART:' + FormatLocalTime(CalculateTaskTime(BaseDateTime,
         Settings.TimeOffsetPeriod, True, TaskIndex)) + CRLF;
  Result := Result + 'DTEND:' + FormatLocalTime(CalculateTaskTime(BaseDateTime,
         Settings.TimeOffsetPeriod, False, TaskIndex)) + CRLF;
  Result := Result + 'DTSTAMP:' + FormatLocalTime(NowUTC) + CRLF;
  Result := Result + 'CLASS:PRIVATE' + CRLF; // event details not visible to others
  Result := Result + 'TRANSP:TRANSPARENT' + CRLF; // show calendar as free
  Result := Result + 'BEGIN:VALARM' + CRLF;
  Result := Result + 'ACTION:DISPLAY' + CRLF;
  Result := Result + 'TRIGGER:-PT' + IntToStr(Settings.Alert) + 'M' + CRLF;
  Result := Result + DescriptionFolding('DESCRIPTION:' + Task.Title) + CRLF;
  Result := Result + 'END:VALARM' + CRLF;
  Result := Result + 'END:VEVENT' + CRLF;
end;

function Tasks2iCalendar(Tasks: TTaskList; Settings: TCalendarSettings): String;
var BaseDateTime: TDateTime;
    I: Integer;
begin
  BaseDateTime := CalculateBaseDateTime(NowUTC);
  Result := CalendarHeader();
  for I := 0 to Tasks.Count - 1 do
  begin
    Result := Result + Task2Appointment(Tasks[I], BaseDateTime, I, Settings);
  end;
  Result := Result + CalendarTrailer();
end;

end.

