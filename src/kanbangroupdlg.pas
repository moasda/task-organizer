{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit KanbanGroupDlg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  TaskModel;

type

  { TKanbanGroupForm }

  TKanbanGroupForm = class(TForm)
    CancelButton: TButton;
    OkButton: TButton;
    ICalendarExportCheckBox: TCheckBox;
    CloseTasksCheckBox: TCheckBox;
    ButtonPanel: TPanel;
    TitleEdit: TEdit;
    TitleLabel: TLabel;
    procedure FormShow(Sender: TObject);
  public
    procedure GetSettings(KanbanGroup: TKanbanGroup);
    procedure SetSettings(KanbanGroup: TKanbanGroup);
  end;

var
  KanbanGroupForm: TKanbanGroupForm;

implementation

uses Constants, MainWindow;

{$R *.lfm}

procedure TKanbanGroupForm.FormShow(Sender: TObject);
begin
  ICalendarExportCheckBox.Enabled := MainForm.IniPropStorage.ReadBoolean(SettingIcalEnabled, SettingIcalEnabledDefault);;
end;

procedure TKanbanGroupForm.GetSettings(KanbanGroup: TKanbanGroup);
begin
  KanbanGroup.Title := TitleEdit.Caption;
  KanbanGroup.CloseTasks := CloseTasksCheckBox.Checked;
  KanbanGroup.ICalendarExport := ICalendarExportCheckBox.Checked;
end;

procedure TKanbanGroupForm.SetSettings(KanbanGroup: TKanbanGroup);
begin
  TitleEdit.Caption := KanbanGroup.Title;
  CloseTasksCheckBox.Checked := KanbanGroup.CloseTasks;
  ICalendarExportCheckBox.Checked := KanbanGroup.ICalendarExport;
end;

end.

