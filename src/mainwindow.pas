{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

unit MainWindow;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Fgl, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls,
  StdCtrls, LCLType, Buttons, IniPropStorage, Menus, UniqueInstance, TaskModel,
  HttpCommunication, Types;

type
  TListBoxList = specialize TFPGList<TListBox>;
  TLabelList = specialize TFPGList<TLabel>;

  TMainForm = class(TForm)
    ApplicationProperties: TApplicationProperties;
    BurgerMenu: TPopupMenu;
    BurgerMenuItemAbout: TMenuItem;
    ImageList: TImageList;
    KanbanGroupMenu: TPopupMenu;
    KanbanGroupMenuEdit: TMenuItem;
    KanbanGroupMenuShowOldTasks: TMenuItem;
    TaskMenuDeleteItem: TMenuItem;
    TaskMenuEditItem: TMenuItem;
    BurgerMenuItemSettings: TMenuItem;
    TaskMenu: TPopupMenu;
    SearchEdit: TEdit;
    IniPropStorage: TIniPropStorage;
    HeaderPanel: TPanel;
    BurgerButton: TSpeedButton;
    UniqueInstance: TUniqueInstance;

    procedure AddButtonClick(Sender: TObject);
    procedure BurgerButtonClick(Sender: TObject);
    procedure BurgerMenuItemAboutClick(Sender: TObject);
    procedure BurgerMenuItemSettingsClick(Sender: TObject);
    procedure ColumnContentDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ColumnContentMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ColumnContentClick(Sender: TObject);
    procedure ColumnContentDblClick(Sender: TObject);
    procedure ColumnContentMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ColumnContentDrawItem(Control: TWinControl;
              Index: Integer; ARect: TRect; State: TOwnerDrawState);
    procedure ColumnPanelDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ColumnPanelDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ColumnContentEndDrag(Sender, Target: TObject; X,Y: Integer);
    procedure ColumnTitleMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure KanbanGroupMenuEditClick(Sender: TObject);
    procedure KanbanGroupMenuShowOldTasksClick(Sender: TObject);
    procedure TaskMenuDeleteItemClick(Sender: TObject);
    procedure TaskMenuEditItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SearchEditChange(Sender: TObject);
    procedure SearchEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SearchEditKeyPress(Sender: TObject; var Key: char);
    procedure UniqueInstanceOtherInstance(Sender: TObject; ParamCount: Integer;
              const Parameters: array of String);
    procedure VisualizeDragMode(NewInDragMode: Boolean);

  public
    TaskManager: TTaskManager;

  private
    KanbanGroupColumnContentList: TListBoxList;
    KanbanGroupColumnTitleList: TLabelList;
    CtrlPressed: Boolean;
    HttpServer: THttpServer;
    TasksFilePath: String;
    InDragMode: Boolean;

    procedure UpdateKanbanGroupColumnsInGui;
    function GetKanbanGroupIndex(ColumnContent: TListBox): Integer;
    function GetColumnContent(KanbanGroupIndex: Integer): TListBox;
    function GetColumnTitle(KanbanGroupIndex: Integer): TLabel;
    procedure EditTask(TaskId: Integer);
    procedure WriteTasksFile;
    procedure ReadTasksFile;
    procedure UpdateTasksInGui;
    function IsTaskRelevant(Task: TTask; ShowOldTasksDate: TDateTime): Boolean;
    procedure InsertTask(Task: TTask; ColumnContent: TListBox);
    procedure RemoveTask(Task: TTask; ColumnContent: TListBox);
    procedure UnselectOtherColumns(ColumnContent: TListBox);
    procedure OnHttpRequest(Sender: TObject; HttpRequest: THttpRequest;
              var HttpResponse: THttpResponse);
  end;

var
  MainForm: TMainForm;

implementation

uses LCLTranslator,
{$IFDEF MSWINDOWS}
  TextUtils,
{$ENDIF}
  TaskEditDlg, KanbanGroupDlg, AboutDlg, SettingsDlg, TaskCalendar,
  Constants, TypInfo;

{$R *.lfm}

resourcestring
  SDeleteTaskCaption       = 'Delete task';
  SDeleteTaskMessage       = 'Do you want to delete the task "%s"?';
  SAddTaskButtonHint       = 'Add task to %s';

procedure TMainForm.FormCreate(Sender: TObject);
var Language, DefaultLang: String;
begin
  IniPropStorage.IniFileName := ConcatPaths([GetAppConfigDir(False), SettingsFilename]);
  WriteLog('Settings file path: ' + IniPropStorage.IniFileName);

  TasksFilePath := ConcatPaths([IniPropStorage.ReadString(SettingWorkFolder, GetUserDir), TasksFilename]);
  WriteLog('Tasks file path: ' + TasksFilePath);

  Language := IniPropStorage.ReadString(SettingLanguage, 'en');
  WriteLog('Set configured language: ' + Language);
  DefaultLang := SetDefaultLang(Language);
  WriteLog('SetDefaultLang returns: ' + DefaultLang);
  SearchEdit.TextHint := SearchEdit.Hint;

  TaskManager := TTaskManager.Create;
  KanbanGroupColumnTitleList := TLabelList.Create;
  KanbanGroupColumnContentList := TListBoxList.Create;

  // Show tasks
  ReadTasksFile;
  UpdateKanbanGroupColumnsInGui;
  UpdateTasksInGui;

  // HTTP server for iCalendar usage
  HttpServer := THttpServer.Create;
  HttpServer.OnHttpRequest := @OnHttpRequest;
  HttpServer.Port := IniPropStorage.ReadInteger(SettingIcalPort, SettingIcalPortDefault);
  if IniPropStorage.ReadBoolean(SettingIcalEnabled, False) then
  begin
    HttpServer.Start;
  end;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  HttpServer.Stop;
  FreeAndNil(HttpServer);
  FreeAndNil(TaskManager);
  FreeAndNil(KanbanGroupColumnTitleList);
  FreeAndNil(KanbanGroupColumnContentList);
end;

procedure TMainForm.UniqueInstanceOtherInstance(Sender: TObject;
          ParamCount: Integer; const Parameters: array of String);
begin
  Show;
  if WindowState = wsMinimized then
  begin
    WindowState := TWindowState.wsNormal;
  end;
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
  // Transparency
  AlphaBlendValue := IniPropStorage.ReadInteger(SettingTransparency, SettingTransparencyDefault);
end;

procedure TMainForm.FormShow(Sender: TObject);
const Part = 10;
begin
  // Ensure window is visible
  if (Left + Width) < (Screen.DesktopLeft + Part) then
  begin
    Left := Screen.DesktopLeft;
  end;
  if Left > (Screen.DesktopLeft + Screen.DesktopWidth - Part) then
  begin
    Left := Screen.DesktopLeft + Screen.DesktopWidth - Width;
  end;
  if (Top + Height) < (Screen.DesktopTop + Part) then
  begin
    Top := Screen.DesktopTop;
  end;
  if Top > (Screen.DesktopTop + Screen.DesktopHeight - Part) then
  begin
    Top := Screen.DesktopTop + Screen.DesktopHeight - Height;
  end;
end;

procedure TMainForm.FormResize(Sender: TObject);
var ColumnCount, ColumnWidth, X: Integer;
    ColumnContent: TListBox;
    ColumnPanel: TWinControl;
begin
  ColumnCount := KanbanGroupColumnContentList.Count;
  ColumnWidth := (Self.ClientWidth - (ColumnCount + 1)*8) div ColumnCount;
  X := 8;
  for ColumnContent in KanbanGroupColumnContentList do
  begin
    ColumnPanel := ColumnContent.Parent;
    ColumnPanel.Left := X;
    X := X + ColumnWidth + 8;
    ColumnPanel.Top := HeaderPanel.ClientRect.Bottom + 8;
    ColumnPanel.Width := ColumnWidth;
    ColumnPanel.Height := ClientHeight - 4 - ColumnPanel.Top;
  end;
end;

{
  Paint a task item in a ListBox.
}
procedure TMainForm.ColumnContentDrawItem(Control: TWinControl;
  Index: Integer; ARect: TRect; State: TOwnerDrawState);
var ListBox: TListBox;
    Task: TTask;
    KanbanGroup: TKanbanGroup;
    OverDueDate: Boolean;
    DueDays: Integer;
    DueText1, DueText2: String;
    DueTextRect: TRect;
begin
  ListBox := Control as TListBox;
  Task := ListBox.Items.Objects[Index] as TTask;
  if Task = nil then
  begin
    // This can happen because TListBox.AddItem() is not atomar
    Exit;
  end;
  KanbanGroup := TaskManager.GetKanbanGroups[Task.KanbanGroupIndex];
  OverDueDate := (Task.DueDate > 0) and (Task.DueDate < Now) and not KanbanGroup.CloseTasks;
  with ListBox.Canvas do
  begin
    // ListBox background color
    Brush.Color := clSilver;
    FillRect(ARect);
    // Don't paint on last line to generate a small space between items
    ARect.Height := ARect.Height - 1;
    // Item background
    Brush.Color := specialize IfThen<TColor>(OverDueDate, clYellowLight, clGreenLight);
    FillRect(ARect);
    // Item text
    Font.Color := clBlack;
    TextOut(ARect.Left + 4, ARect.Top + 1, ListBox.Items[Index]);
    // Due date symbol
    if (Task.DueDate > 0) and not KanbanGroup.CloseTasks then
    begin
      DueText1 := uClock;
      DueText2 := '';
      DueDays := Trunc(Task.DueDate) - Trunc(Now);
      if (DueDays >= 1) and (DueDays <= 9) then
      begin
        DueText2 := '-' + IntToStr(DueDays);
        Brush.Color := clYellowLight;
      end;
      DueTextRect := Rect(ARect.Right - 4 - TextWidth(DueText1) - TextWidth(DueText2),
        ARect.Top + 1, ARect.Right, ARect.Bottom - 1);
      if DueText2 <> '' then
        DueTextRect.Left := DueTextRect.Left - 2;
      FillRect(DueTextRect);
      TextRect(DueTextRect, DueTextRect.Left + 1, DueTextRect.Top, DueText1, TextStyle);
      // Paint number separately to avoid smaller text size
      if DueText2 <> '' then
        TextRect(DueTextRect, DueTextRect.Left + 3 + TextWidth(DueText1), DueTextRect.Top, DueText2, TextStyle);
    end;
    // Item border
    Pen.Color := specialize IfThen<TColor>(OverDueDate, clYellowDark, clGreenDark);
    if odSelected in State then
    begin
      Pen.Width := 3;
      Frame(ARect.Left + 1, ARect.Top + 1, ARect.Right - 1, ARect.Bottom - 1);
    end
    else
    begin
      Pen.Width := 1;
      Frame(ARect);
    end;
  end;
end;

procedure TMainForm.ColumnContentClick(Sender: TObject);
begin
  UnselectOtherColumns(Sender as TListBox);
end;

procedure TMainForm.ColumnContentDblClick(Sender: TObject);
var ListBox: TListBox;
    MouseRel: TPoint;
    I, TaskId: Integer;
begin
  ListBox := Sender as TListBox;
  MouseRel := ListBox.ScreenToClient(Mouse.CursorPos);
  I := ListBox.GetIndexAtXY(MouseRel.X, MouseRel.Y);
  if I >= 0 then
  begin
    TaskId := (ListBox.Items.Objects[I] as TTask).Id;
    EditTask(TaskId);
  end;
end;

procedure TMainForm.ColumnContentMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var I: Integer;
begin
  // Start dragging after mouse move
  I := (Sender as TListBox).GetIndexAtXY(X, Y);
  if (Button = mbLeft) and (I >= 0) then
  begin
    (Sender as TListBox).BeginDrag(False, 5);
  end;
end;

procedure TMainForm.ColumnContentMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var ListBox: TListBox;
    MouseAbs: TPoint;
    I: Integer;
begin
  // Context menu on right mouse button
  ListBox := Sender as TListBox;
  I := ListBox.GetIndexAtXY(X, Y);
  if (Button = mbRight) and (I >= 0) then
  begin
    MouseAbs := ListBox.ClientToScreen(Point(X, Y));
    TaskMenu.Tag := (ListBox.Items.Objects[I] as TTask).Id;
    TaskMenu.PopUp(MouseAbs.X, MouseAbs.Y);
  end;
end;

{
  Is called from ListBox during dragging an item.
  Sender: Control that triggered the event.
  Source: Control that initiated the Drag&Drop operation.
}
procedure TMainForm.ColumnContentDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  VisualizeDragMode(True);
end;

{
  Is called from background panel during dragging an item.
  Sender: Control that triggered the event.
  Source: Control that initiated the Drag&Drop operation.
}
procedure TMainForm.ColumnPanelDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := True;
  // Highlight target column
  if (State = dsDragEnter) or (State = dsDragMove) then
  begin
    (Sender as TPanel).Color := clGreenLightMedium;
  end
  else
  begin
    (Sender as TPanel).Color := clGreenLight;
  end;
end;

{
  Is called from background panel when dropping an item.
  Sender: Control that triggered the event.
  Source: Control that initiated the Drag&Drop operation.
}
procedure TMainForm.ColumnPanelDragDrop(Sender, Source: TObject; X, Y: Integer);
var SourceColumnContent, TargetColumnContent: TListBox;
    BackgroundPanel: TPanel;
    I: Integer;
    Task: TTask;
    KanbanGroup: TKanbanGroup;
begin
  // Find source and target ListBox
  SourceColumnContent := Source as TListBox;
  BackgroundPanel := Sender as TPanel;
  for I := 0 to BackgroundPanel.ControlCount - 1 do
  begin
    if BackgroundPanel.Controls[I] is TListBox then
    begin
      TargetColumnContent := BackgroundPanel.Controls[I] as TListBox;
    end;
  end;
  if SourceColumnContent <> TargetColumnContent then
  begin
    // Move item in GUI
    Task := SourceColumnContent.Items.Objects[SourceColumnContent.ItemIndex] as TTask;
    RemoveTask(Task, SourceColumnContent);
    InsertTask(Task, TargetColumnContent);
    TargetColumnContent.ItemIndex := TargetColumnContent.Items.IndexOfObject(Task);
    // Update task
    Task.KanbanGroupIndex := GetKanbanGroupIndex(TargetColumnContent);
    KanbanGroup := TaskManager.GetKanbanGroups[Task.KanbanGroupIndex];
    if not KanbanGroup.CloseTasks and (Task.Status in [Completed, Cancelled]) then
    begin
      Task.Status := InProcess;
    end;
    if KanbanGroup.CloseTasks and (Task.Status <> Cancelled) then
    begin
      Task.Status := Completed;
    end;
    if KanbanGroup.CloseTasks then
    begin
      Task.CompletedDate := Task.LastModifiedDate;
    end;
    Task.LastModifiedDate := Now;
    WriteTasksFile;
  end;
end;

{
  Is called from ListBox after dragging has ended.
}
procedure TMainForm.ColumnContentEndDrag(Sender, Target: TObject; X,Y: Integer);
begin
  VisualizeDragMode(False);
end;

{
  Context menu for column title.
}
procedure TMainForm.ColumnTitleMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var ColumnTitle: TLabel;
    KanbanGroupIndex: Integer;
    KanbanGroup: TKanbanGroup;
    ShowOldTasksEnabled: Boolean;
    MouseAbs: TPoint;
begin
  // Context menu on right mouse button
  ColumnTitle := Sender as TLabel;
  if Button = mbRight then
  begin
    // Set KanbanGroupIndex
    KanbanGroupIndex := StrToInt(ColumnTitle.Parent.Hint);
    KanbanGroupMenu.Tag := KanbanGroupIndex;
    // Update menu item visibility
    KanbanGroup := TaskManager.GetKanbanGroups[KanbanGroupIndex];
    KanbanGroupMenuShowOldTasks.Enabled := KanbanGroup.CloseTasks;
    ShowOldTasksEnabled := IniPropStorage.ReadBoolean(SettingShowOldTasksEnabled, SettingShowOldTasksEnabledDefault);
    KanbanGroupMenuShowOldTasks.Checked := ShowOldTasksEnabled and KanbanGroupMenuShowOldTasks.Enabled;
    // Show menu
    MouseAbs := ColumnTitle.ClientToScreen(Point(X, Y));
    KanbanGroupMenu.PopUp(MouseAbs.X, MouseAbs.Y);
  end;
end;

{
  Show green empty columns as drag targets during dragging.
}
procedure TMainForm.VisualizeDragMode(NewInDragMode: Boolean);
var ColumnContent: TListBox;
begin
  if NewInDragMode = InDragMode then
  begin
    Exit;
  end;
  InDragMode := NewInDragMode;
  for ColumnContent in KanbanGroupColumnContentList do
  begin
    ColumnContent.Visible := not NewInDragMode;
  end;
end;

procedure TMainForm.AddButtonClick(Sender: TObject);
var AddButton: TControl;
    KanbanGroupIndex: Integer;
    Task: TTask;
    FinalColumnContent: TListBox;
begin
  AddButton := Sender as TControl;
  KanbanGroupIndex := StrToInt(AddButton.Parent.Parent.Hint);

  TaskEditForm.CleanFields(TaskManager.GetKanbanGroups, KanbanGroupIndex);
  if TaskEditForm.ShowModal = mrOK then
  begin
    Task := TaskEditForm.GetTask;
    TaskManager.AddTask(Task);
    WriteTasksFile;
    FinalColumnContent := GetColumnContent(Task.KanbanGroupIndex);
    InsertTask(Task, FinalColumnContent);
    // Highlight new task
    UnselectOtherColumns(FinalColumnContent);
    FinalColumnContent.ItemIndex := FinalColumnContent.Items.IndexOfObject(Task);
  end;
end;

procedure TMainForm.UpdateKanbanGroupColumnsInGui;
var KanbanGroup: TKanbanGroup;
    ColumnPanel: TPanel;
    ColumnName: TLabel;
    ColumnContent: TListBox;
    ColumnFooterPanel: TPanel;
    AddButton: TSpeedButton;
begin
  // Delete all kanban groups
  for ColumnContent in KanbanGroupColumnContentList do
  begin
    ColumnContent.Parent.Free; // Destroy parent panel
  end;
  KanbanGroupColumnContentList.Clear;
  KanbanGroupColumnTitleList.Clear;
  // Create new kanban groups
  for KanbanGroup in TaskManager.GetKanbanGroups do
  begin
    ColumnPanel := TPanel.Create(Self);
    ColumnPanel.Parent := Self;
    ColumnPanel.BevelOuter := bvNone;
    ColumnPanel.BorderSpacing.Around := 8;
    // Drag&Drop support
    ColumnPanel.Color := clGreenLight;
    ColumnPanel.Hint := IntToStr(KanbanGroup.Index);
    ColumnPanel.OnDragDrop := @ColumnPanelDragDrop;
    ColumnPanel.OnDragOver := @ColumnPanelDragOver;
    ColumnName := TLabel.Create(ColumnPanel);
    ColumnName.Parent := ColumnPanel;
    ColumnName.Align := alTop;
    ColumnName.AutoSize := False;
    if (KanbanGroup.Title <> '') then
      ColumnName.Caption := KanbanGroup.Title
    else
      ColumnName.Caption := GetDefaultGroupTitle(KanbanGroup.Index);
    ColumnName.Color := clForm;
    ColumnName.Font.Style := ColumnName.Font.Style + [fsBold];
    ColumnName.Height := ScaleDesignToForm(17);
    ColumnName.Transparent := False;
    ColumnName.OnMouseUp := @ColumnTitleMouseUp;
    ColumnContent := TListBox.Create(ColumnPanel);
    ColumnContent.Parent := ColumnPanel;
    ColumnContent.Align := alClient;
    ColumnContent.BorderStyle := bsNone;
    ColumnContent.Color := clSilver;
    ColumnContent.Hint := IntToStr(KanbanGroup.Index);
    ColumnContent.ItemHeight := ScaleDesignToForm(20);
    ColumnContent.Sorted := True;
    ColumnContent.Style := lbOwnerDrawFixed;
    ColumnContent.OnClick := @ColumnContentClick;
    ColumnContent.OnDblClick := @ColumnContentDblClick;
    ColumnContent.OnDragOver := @ColumnContentDragOver;
    ColumnContent.OnDrawItem := @ColumnContentDrawItem;
    ColumnContent.OnEndDrag := @ColumnContentEndDrag;
    ColumnContent.OnMouseDown := @ColumnContentMouseDown;
    ColumnContent.OnMouseUp := @ColumnContentMouseUp;
    ColumnFooterPanel := TPanel.Create(ColumnPanel);
    ColumnFooterPanel.Parent := ColumnPanel;
    ColumnFooterPanel.Align := alBottom;
    ColumnFooterPanel.BevelOuter := bvNone;
    ColumnFooterPanel.Color := clForm;
    ColumnFooterPanel.Height := ScaleDesignToForm(22);
    if not KanbanGroup.CloseTasks then
    begin
      AddButton := TSpeedButton.Create(ColumnFooterPanel);
      AddButton.Parent := ColumnFooterPanel;
      AddButton.Align := alRight;
      AddButton.Caption := '+';
      AddButton.Flat := True;
      AddButton.Font.Color := clMaroon;
      AddButton.Font.Style := AddButton.Font.Style + [fsBold];
      AddButton.Height := ScaleDesignToForm(22);
      AddButton.Hint := Format(SAddTaskButtonHint, [KanbanGroup.Title]);
      AddButton.ShowHint := True;
      AddButton.OnClick := @AddButtonClick;
    end;
    KanbanGroupColumnTitleList.Add(ColumnName);
    KanbanGroupColumnContentList.Add(ColumnContent);
  end;
end;

function TMainForm.GetKanbanGroupIndex(ColumnContent: TListBox): Integer;
begin
  Result := StrToInt(ColumnContent.Hint);
end;

function TMainForm.GetColumnContent(KanbanGroupIndex: Integer): TListBox;
begin
  Result := KanbanGroupColumnContentList[KanbanGroupIndex];
end;

function TMainForm.GetColumnTitle(KanbanGroupIndex: Integer): TLabel;
begin
  Result := KanbanGroupColumnTitleList[KanbanGroupIndex];
end;

procedure TMainForm.EditTask(TaskId: Integer);
var OldTask, NewTask: TTask;
    OldTaskKanbanGroupIndex: Integer;
    ColumnContent: TListBox;
begin
  OldTask := TaskManager.GetTask(TaskId);
  TaskEditForm.SetTask(OldTask, TaskManager.GetKanbanGroups);
  if TaskEditForm.ShowModal = mrOK then
  begin
    NewTask := TaskEditForm.GetTask;
    OldTaskKanbanGroupIndex := OldTask.KanbanGroupIndex;
    OldTask.UpdateTask(NewTask);
    OldTask.LastModifiedDate := Now;
    NewTask.Free;
    WriteTasksFile;
    // Update UI
    ColumnContent := GetColumnContent(OldTaskKanbanGroupIndex);
    RemoveTask(OldTask, ColumnContent);
    ColumnContent := GetColumnContent(OldTask.KanbanGroupIndex);
    InsertTask(OldTask, ColumnContent);
    ColumnContent.ItemIndex := ColumnContent.Items.IndexOfObject(OldTask);
  end;
end;

procedure TMainForm.WriteTasksFile;
begin
    TaskManager.WriteToFile(TasksFilePath);
end;

procedure TMainForm.ReadTasksFile;
begin
  try
    TaskManager.ReadFromFile(TasksFilePath);
  except
    on e: Exception do
      WriteLog('Error reading tasks file: ' + e.Message);
  end;
end;

procedure TMainForm.UpdateTasksInGui;
var ColumnContent: TListBox;
    ShowOldTasksEnabled: Boolean;
    ShowOldTasksDays: Integer;
    ShowOldTasksDate: TDateTime;
    I: Integer;
    Task: TTask;
    TaskVisible: Boolean;
begin
  // Clear columns
  for ColumnContent in KanbanGroupColumnContentList do
  begin
    ColumnContent.Clear;
  end;
  // Fill columns
  ShowOldTasksEnabled := IniPropStorage.ReadBoolean(SettingShowOldTasksEnabled, SettingShowOldTasksEnabledDefault);
  if ShowOldTasksEnabled then
  begin
    ShowOldTasksDate := 0;
  end
  else
  begin
    ShowOldTasksDays := IniPropStorage.ReadInteger(SettingShowOldTasksDays, SettingShowOldTasksDaysDefault);
    ShowOldTasksDate := Now - ShowOldTasksDays;
  end;
  for I := 0 to TaskManager.GetTasks.Count - 1 do
  begin
    Task := TaskManager.GetTasks[I];
    TaskVisible := (SearchEdit.Text = '') or ContainsText(SearchEdit.Text, Task);
    if IsTaskRelevant(Task, ShowOldTasksDate) and TaskVisible then
    begin
      ColumnContent := GetColumnContent(Task.KanbanGroupIndex);
      InsertTask(Task, ColumnContent);
    end;
  end;
end;

{
  Returns if a task should be shown on the board.
}
function TMainForm.IsTaskRelevant(Task: TTask; ShowOldTasksDate: TDateTime): Boolean;
var KanbanGroup: TKanbanGroup;
begin
  KanbanGroup := TaskManager.GetKanbanGroups[Task.KanbanGroupIndex];
  Result := not KanbanGroup.CloseTasks or (Task.LastModifiedDate = 0) or
    (Task.LastModifiedDate > ShowOldTasksDate);
end;

procedure TMainForm.InsertTask(Task: TTask; ColumnContent: TListBox);
begin
  ColumnContent.AddItem(Task.Title, Task);
end;

procedure TMainForm.RemoveTask(Task: TTask; ColumnContent: TListBox);
var I: Integer;
begin
  I := ColumnContent.Items.IndexOfObject(Task);
  if I >= 0 then
  begin
    ColumnContent.Items.Delete(I);
  end;
end;

{
  Unselect tasks in all list boxes other than the given one.
}
procedure TMainForm.UnselectOtherColumns(ColumnContent: TListBox);
var ListBox: TListBox;
begin
  for ListBox in KanbanGroupColumnContentList do
  begin
    if ListBox <> ColumnContent then
    begin
      ListBox.ItemIndex := -1;
    end;
  end;
end;

procedure TMainForm.SearchEditChange(Sender: TObject);
begin
  UpdateTasksInGui;
end;

procedure TMainForm.SearchEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  CtrlPressed := ssCtrl in Shift;
end;

procedure TMainForm.SearchEditKeyPress(Sender: TObject; var Key: char);
begin
  // ESC key pressed, clear input
  if Key = #27 then
  begin
    SearchEdit.Text := '';
    Key := #0;
  end;
  {$IFDEF MSWINDOWS}
  // Ctrl+Backspace pressed, delete word before cursor
  // Windows sends #127, Linux sends #8 = VK_BACK
  if Key in [#127] then
  begin
    DeleteWordBeforeCursor(SearchEdit);
    Key := #0;
  end;
  {$ENDIF}
end;

procedure TMainForm.FormKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #27 then
  begin
    SearchEdit.SetFocus;
    SearchEditKeyPress(Sender, Key);
  end;
end;

procedure TMainForm.BurgerButtonClick(Sender: TObject);
var
  LowerRight: TPoint;
begin
  if Sender is TControl then
  begin
    LowerRight := Point(BurgerButton.Left + BurgerButton.Width, BurgerButton.Top + BurgerButton.Height);
    LowerRight := ClientToScreen(LowerRight);
    BurgerMenu.Popup(LowerRight.X, LowerRight.Y);
  end;
end;

procedure TMainForm.BurgerMenuItemAboutClick(Sender: TObject);
begin
  AboutForm.ShowModal;
end;

procedure TMainForm.BurgerMenuItemSettingsClick(Sender: TObject);
var OldAlphaBlendValue: Integer;
begin
  OldAlphaBlendValue := AlphaBlendValue;
  if SettingsForm.ShowModal = mrOK then
  begin
    WriteTasksFile;
    // Update content in case of language etc. was changed
    UpdateKanbanGroupColumnsInGui();
    UpdateTasksInGui;
    FormResize(nil);
    // Update HTTP server configuration
    HttpServer.Stop;
    HttpServer.Port := IniPropStorage.ReadInteger(SettingIcalPort, SettingIcalPortDefault);
    if IniPropStorage.ReadBoolean(SettingIcalEnabled, False) then
      HttpServer.Start;
  end
  else
  begin
    AlphaBlendValue := OldAlphaBlendValue;
  end;
end;

procedure TMainForm.KanbanGroupMenuEditClick(Sender: TObject);
var KanbanGroupIndex: Integer;
    KanbanGroup: TKanbanGroup;
begin
  KanbanGroupIndex := KanbanGroupMenu.Tag;
  KanbanGroup := TaskManager.GetKanbanGroups[KanbanGroupIndex];
  KanbanGroupForm.SetSettings(KanbanGroup);
  if KanbanGroupForm.ShowModal = mrOk then
  begin
    KanbanGroupForm.GetSettings(KanbanGroup);
    WriteTasksFile;
    UpdateKanbanGroupColumnsInGui();
    UpdateTasksInGui;
    FormResize(nil);
  end;
end;

procedure TMainForm.KanbanGroupMenuShowOldTasksClick(Sender: TObject);
var ShowOldTasksEnabled: Boolean;
begin
  ShowOldTasksEnabled := IniPropStorage.ReadBoolean(SettingShowOldTasksEnabled, SettingShowOldTasksEnabledDefault);
  ShowOldTasksEnabled := Not ShowOldTasksEnabled;
  IniPropStorage.WriteBoolean(SettingShowOldTasksEnabled, ShowOldTasksEnabled);
  UpdateTasksInGui;
end;

procedure TMainForm.TaskMenuEditItemClick(Sender: TObject);
begin
  if TaskMenu.Tag <> 0 then
  begin
    EditTask(TaskMenu.Tag);
  end;
end;

procedure TMainForm.TaskMenuDeleteItemClick(Sender: TObject);
var TaskId: Integer;
    Task: TTask;
    Msg: String;
    ColumnContent: TListBox;
begin
  TaskId := TaskMenu.Tag;
  if TaskId = 0 then
  begin
    Exit;
  end;
  Task := TaskManager.GetTask(TaskId);
  Msg := Format(SDeleteTaskMessage, [Task.Title]);
  if MessageDlg(SDeleteTaskCaption, Msg, TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    WriteLog('Delete task: ' + Task.Title);
    // Remove from GUI
    ColumnContent := GetColumnContent(Task.KanbanGroupIndex);
    RemoveTask(Task, ColumnContent);
    // Delete task
    TaskManager.DeleteTask(TaskId);
    WriteTasksFile;
  end;
end;

procedure TMainForm.OnHttpRequest(Sender: TObject; HttpRequest: THttpRequest;
          var HttpResponse: THttpResponse);
var TodayTasks: TTaskList;
    CalendarSettings: TCalendarSettings;
begin
  WriteLog('OnHttpRequest was called');
  if HttpRequest.Path = CalendarUrlPath then
  begin
    // Read calendar settings
    CalendarSettings.TimeOffsetPeriod := IniPropStorage.ReadInteger(SettingIcalTimeOffset, SettingIcalTimeOffsetDefault);
    CalendarSettings.Alert := IniPropStorage.ReadInteger(SettingIcalAlert, SettingIcalAlertDefault);
    // Format calendar
    HttpResponse.StatusCode := 200; // ok
    HttpResponse.ContentType := 'text/calendar';
    TodayTasks := TaskManager.GetICalendarTasks;
    HttpResponse.Data := Tasks2iCalendar(TodayTasks, CalendarSettings);
    WriteLog('OnHttpRequest ' + CalendarUrlPath + ': ' + IntToStr(TodayTasks.Count) + ' calendar entries');
    TodayTasks.Free;
  end
  else
  begin
    HttpResponse.StatusCode := 404; // not found
  end;
end;

end.

