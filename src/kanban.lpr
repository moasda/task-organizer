{
  Personal Kanban Task Organizer
  Copyright (C) 2020 - 2024 Herbert Reiter

  This program is free software: You can redistribute it and/or modify it
  under the terms of the GNU General Public License version 3 as published
  by the Free Software Foundation (GPL-3.0-only).

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
}

program kanban;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, LazLogger, datetimectrls, JsonTools, LCLTranslator, SysUtils,
  Constants, TextUtils, TaskModel,
  MainWindow, TaskEditDlg, AboutDlg, SettingsDlg, KanbanGroupDlg;

{$R *.res}

begin
  WriteLog('Starting Personal Kanban Task Organizer ' + GetProgramVersion);
  WriteLog('Binary: ' + Application.ExeName);
  RequireDerivedFormResource := True;
  Application.Title:='Personal Kanban Task Organizer';
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TTaskEditForm, TaskEditForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.CreateForm(TSettingsForm, SettingsForm);
  Application.CreateForm(TKanbanGroupForm, KanbanGroupForm);
  Application.Run;
  WriteLog('Stopping application');
end.

